function guiData = gui_HltVsAlpha(closeCallback, linkDynamicsCallback)
% GUI_HLTVSALPHA Display an interface that allows the user to select alpha
% and Hlt from the ZAC energy curve representation
%
%   guiData = gui_HltVsAlpha(closeCallback, linkDynamicsCallback)
%
%   Inputs:
%
%       closeCallback           -   Handle to a function that should be
%                                   assigned to the CloseRequestFcn
%                                   property of the figure
%       linkDynamicsCallback    -   Handle to a function that updates the
%                                   alpha and Hlt values of the core
%                                   environment
%
%   Outputs: 
%
%       guiData     -   Returns handles to the figure, the axes, and a
%                       function that updates the figure by querying the 
%                       global sharedData variable
%
%   Author: Andrew Cox
%   Version: September 24, 2018

    global sharedData;

    h_fig = figure('color', 'w', 'position', [50, 50, 500, 600],...
        'closeRequestFcn', closeCallback, 'visible', 'off');
    movegui(h_fig, 'east');
    
    h_ax = axes(h_fig, 'units', 'pixels', 'position', [85, 125, 390, 450]);
    
    uicontrol(h_fig, 'style', 'pushbutton', 'String', 'Pick Point',...
        'units', 'normalized', 'position', [0.45, 0.05, 0.2, 0.05],...
        'callback', @pickHltAlphaCallback);

    set(h_ax, 'units', 'normalized');
    xlabel(h_ax, '$\alpha$');
    ylabel(h_ax, '$H_{lt}$');
    
    gfx = getDefaultDisplay;
    set(h_fig, 'defaultTextInterpreter', gfx.interp);

    set(h_fig, 'defaultFigureColor', gfx.fig_bgcolor);

    set(h_fig, 'defaultAxesFontSize', gfx.fontsize);
    set(h_fig, 'defaultAxesFontName', gfx.fontname);
    set(h_fig, 'defaultAxesFontWeight', gfx.fontweight);
    set(h_fig, 'defaultAxesGridAlpha', 0.3);
    set(h_fig, 'defaultAxesTickLabelInterpreter', gfx.interp);

    set(h_fig, 'defaultColorbarFontSize', gfx.fontsize);
    set(h_fig, 'defaultColorbarFontName', gfx.fontname);
    set(h_fig, 'defaultColorbarFontWeight', gfx.fontweight);
    set(h_fig, 'defaultColorbarTickLabelInterpreter', gfx.interp);

    set(h_fig, 'defaultLegendFontSize', gfx.fontsize);
    set(h_fig, 'defaultLegendFontName', gfx.fontname);
    set(h_fig, 'defaultLegendFontWeight', gfx.fontweight);
    set(h_fig, 'defaultLegendInterpreter', gfx.interp);
    set(h_fig, 'defaultLegendLocation', 'best');

    guiData = struct('h_fig', h_fig, 'h_ax', h_ax, 'updateFcn', @updatePlot);
    set(h_fig, 'visible', 'on');
    
%% Functions
    function pickHltAlphaCallback(~,~)
        [a, Hlt] = ginput(1);
        alpha = a*pi/180;
        linkDynamicsCallback([], alpha, Hlt);
    end

    function updatePlot()

        if(strcmpi(h_fig.Visible, 'on'))
            delete(get(h_ax, 'children'));
            hold(h_ax, 'on');
            % Plot the equilibria and ZAC data
            for L = 1:length(sharedData.ltZACs.pos)
                if(sharedData.ltZACs.colorMode == 3)
                    scatter(h_ax, wrapToPi(sharedData.ltZACs.pos{L}(:,1))*180/pi, ...
                        sharedData.ltZACs.Hlt{L}, 8, ...
                        sharedData.ltZACs.col_stab(sharedData.ltZACs.stab{L},:),...
                        'filled');
                else
                    plot(h_ax, wrapToPi(sharedData.ltZACs.pos{L}(:,1))*180/pi, ...
                        sharedData.ltZACs.Hlt{L}, '.',...
                        'color', sharedData.ltZACs.col_default(L,:));
                end
            end

            if(sharedData.ltZACs.colorMode == 3)
                scatter(h_ax, wrapToPi(sharedData.ltEqPts.pos(:,1))*180/pi,...
                    sharedData.ltEqPts.Hlt, 64, sharedData.ltZACs.col_stab(eqPtStab,:),...
                    'filled', 'marker', 'd');
            else
                plot(h_ax, wrapToPi(sharedData.ltEqPts.pos(:,1))*180/pi,...
                    sharedData.ltEqPts.Hlt, 'kd', 'markerfacecolor', 'k');
            end

            grid(h_ax, 'on');
        end
    end
end

