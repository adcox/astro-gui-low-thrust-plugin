function saddles = cr3bp_lt_getZACSaddles(mu, varargin)
%% CR3BP_LT_GETZACSADDLES Compute saddle locations for the CR3BP-LT ZAC structures
%
%   saddles = cr3bp_lt_getZACSaddles(mu) takes the system mass ratio, mu,
%   and computes the locations of the saddles. Each row of the saddles
%   output represents the location of one saddle with the data
%
%       row = [mu, f, x, y]
%
%   where mu is the system mass ratio, f is the thrust magnitude that the
%   saddle exists at, and (x,y) is the saddle position in nondimensional
%   units. The rows are output in the order,
%
%       saddles = [S2/4; S1/4; S1/5; S2/5; S3/4; S3/5]
%
%
%   Additional Argument/Input pairs:
%
%       'minErr'        -   Specify the numerical tolerance (Default = 1e-6)
%   Author: Andrew Cox
%   Version: July 16, 2018

validMu = @(x) isnumeric(x) && isscalar(x) && x > 0 && x <= 0.5;
posNum = @(x) isnumeric(x) && isscalar(x) && x > 0;

%% Parse inputs
p = inputParser;

addRequired(p, 'mu', validMu);

addOptional(p, 'minErr', 1e-6, posNum);

parse(p, mu, varargin{:});

minErr = p.Results.minErr;

%% Do computations
b = 1000;   % Base for logarithm
LPts = cr3bp_getEquilibPts(mu, 'matrix', 'on');
% Initial points for saddles near P2
theta = ((pi/4) : (pi/2) : (2*pi)).';
initPos = [1.2*(1-mu - LPts(1,1))*cos(theta) + 1 - mu,...
    1.2*(1-mu - LPts(1,1))*sin(theta)];

% Add initial points for saddles between L4/5 and L3
theta = [1;-1]*(105*pi/180);
initPos = [initPos; 0.99*cos(theta) - mu, 0.99*sin(theta)];

saddles = zeros(size(initPos, 1), 4);

for s = 1:size(initPos, 1)
    err = inf;
    count = 1;
    maxCount = 100;
    pos = initPos(s,:).';

%         fprintf('Saddle %02d\n-------------\n', s);
    while(err > minErr && count < maxCount)
        [~, Ux, Uy, ~, Uxx, Uyy, ~, Uxy] = cr3bp_getPseudoPot(mu, [pos; 0]);
        x = pos(1);
        y = pos(2);

        r13 = sqrt((x+mu)^2 + y^2);
        r23 = sqrt((x-1+mu)^2 + y^2);

        Uxxx = 9*(1-mu)*(x+mu)/r13^5 + 9*mu*(x-1+mu)/r23^5 -...
            15*(1-mu)*(x+mu)^3/r13^7 - 15*mu*(x-1+mu)^3/r23^7;
        Uxxy = 3*(1-mu)*y/r13^5 + 3*mu*y/r23^5 - 15*(1-mu)*(x+mu)^2*y/r13^7 -...
            15*mu*(x-1+mu)^2*y/r23^7;
        Uyyy = 9*(1-mu)*y/r13^5 + 9*mu*y/r23^5 - 15*(1-mu)*y^3/r13^7 - 15*mu*y^3/r23^7;
        Uyyx = 3*(1-mu)*(x+mu)/r13^5 + 3*mu*(x-1+mu)/r23^5 -...
            15*(1-mu)*(x+mu)*y^2/r13^7 - 15*mu*(x-1+mu)*y^2/r23^7;

        Uxyx = Uxxy;
        Uxyy = Uyyx;

        A = Ux^2 + Uy^2;    % Acceleration magnitude squared
%             F = log(A)/log(b);       % Surface to find saddles on

        % Gradient of the surface (w.r.t. x and y)
        g = [Uxx*Ux + Uxy*Uy; Uxy*Ux + Uyy*Uy];
        dF = 2/(A*log(b))*g;

        J = [Uxxx*Ux + Uxx^2 + Uxyx*Uy + Uxy^2, Uxxy*Ux + Uxx*Uxy + Uxyy*Uy + Uxy*Uyy;
            Uxyx*Ux + Uxy*Uxx + Uyyx*Uy + Uyy*Uxy, Uxyy*Ux + Uxy^2 + Uyyy*Uy + Uyy^2];
        if(s <= 4)
            ddF = (-4/(log(b)*A^2))*(g*g.') + (2/(log(b)*A))*J;
        else
            % This derivative is WRONG
            % By flipping the g vector, we tell the Newton process to step
            % orthogonal to the actual gradient... this works well for L4/5
            % because the saddle is very narrow and the Newton process
            % quickly diverges when it steps along the P1-Saddle vector
            g2 = [0 1;1 0]*g;
            ddF = (-4/(log(b)*A^2))*(g2*g2.') + (2/(log(b)*A))*J;
        end

        pos = pos - ddF\dF;

        err = norm(dF);
%             fprintf('  It %02d: err = %.2e\n', count, err);
        count = count + 1;    
    end

    if(err <= minErr) 
        qdot = cr3bp_EOMs(0, [x;y;0;0;0;0;zeros(36,1)], mu);
        saddles(s,:) = [mu, sqrt(qdot(4:6).'*qdot(4:6)), x, y];
    else
        warning('Could not converge on saddle %d\n', s);
    end
end

% %%% End of Function %%%
end