function [ value, isTerminal, direction ] = cr3bp_lt_event_mass( t, y, minMass)
%CR3BP_EVENT_CRASH Detect crashes with P1 and P2; for use with ODE solver
%
%   [value, isTerminal, direction] = cr3bp_event_crash(t, y, mu, R1, R2)
%    computes the distance from P3 to either primary and compares it with
%    the specified radii. Inputs: t and y are the time and state (same as
%    used with ode*), mu is the system mass ratio, R1 is the non-dim radius
%    of P1, R2 is the non-dim radius of P2.
%
%   Author: Andrew Cox
%   Version: March 30, 2017

value = y(7) - minMass;
isTerminal = [1];         % Both events end the integration
direction = [0];          % Use positive or negative direction
end

