function s_dot = cr3bp_lt_EOMs(t, s, mu, lawIDs, ctrl, params, charT, charL)
%CR3BP_LT_EOMS Contains Equations of motion for P3 in the low-thrust CR3BP as
%   well as equations for integrating the state transition matrix (STM).
%
%   s_dot = cr3bp_lt_EOMs(t, s, mu, lawID, ctrl, params, charT, charL) 
%   computes the derivatives of the state vector s over the time interval t.
%   Mu is the mass ratio for the system, lawIDs is a vector of decoded
%   binary lawID values, ctrl is the control state vector, params contains
%   constant parameters for the control law, and charT and charL are the 
%   characteristic time and length, respectively, of the CR3BP.
%
%   The state vector takes the form:
%   [x0 y0 z0 vx0 vy0 vz0 m STM(1,1), STM(1,2), STM(1,3), ... STM(7,7)]
%   Initial position and velocity can be determined by the user but
%   the initial state of the STM must be identity, so STM(i,i) equals 1 and
%   all other elements equal 0.
%
%   NOTE: This function should strive to be as fast and efficient as
%   possible as it will be called millions of times during long
%   integrations
%
%   Author: Andrew Cox

x = s(1);
y = s(2);
z = s(3);
d = sqrt((x + mu)^2 + y^2 + z^2);
r = sqrt((x - 1 + mu)^2 + y^2 + z^2);

phiDot = zeros(7,7);

[f, u, mdot] = cr3bp_lt_getCtrlVals(t, s, ctrl, params, lawIDs, charT, charL);

% The derivatives of the state
s_dot = zeros(56,1);
s_dot(1:3) = s(4:6);
s_dot(4) = 2*s(5) + x - (1-mu)*(x + mu)/d^3 - mu*(x - 1 + mu)/r^3 + f/s(7)*u(1);
s_dot(5) = -2*s(4) + y - (1-mu)*y/d^3 - mu*y/r^3 + f/s(7)*u(2);
s_dot(6) = -(1-mu)*z/d^3 - mu*z/r^3 + f/s(7)*u(3);
s_dot(7) = mdot;


s_dot(8:end) = reshape(phiDot,7,7);
end