function guiData = gui_famBuilder(dataDir, closeCallback, loadFamCallback)
% GUI_FAMBUILDER An interface to construct families of LTPOs
%
%   guiData = gui_famBuilder(dataDir, closeCallback, loadFamCallback)
%
%   Inputs:
%
%       dataDir         -   Directory that stores the familes, e.g.,
%                           /GUI_Astrohelion/data
%       closeCallback   -   Handle to a function that should be assigned to
%                           the CloseRequestFcn property of the figure
%       loadFamCallback -   Handle to a function that loads a family from a
%                           file.
%
%   Outputs: 
%
%       guiData     -   Returns handles to the figure, the axes, and a
%                       function that updates the figure by querying the 
%                       global sharedData variable
%
%   Author: Andrew Cox
%   Version: September 24, 2018

    global sharedData;
    
    viewMode = 1;
    zacSelected = 1;
    families = {};
    famFiles = [];
    
    % Plot stuff
    h_toDelete = {};
    colors = lines(5);
    stabColors = [1, 0, 0;
                  0, 0, 1;
                  colors(5,:);
                  0, 0, 0;
                  [1/sqrt(2), 0, 1/sqrt(2)]];
    
    % Create and then hide the GUI as it is being constructed
    screen = get(groot, 'screensize');
    h_fig = figure('Visible', 'off',...
        'position', [0,0, 1000, 700],...
        'color', 'w', 'closeRequestFcn', closeCallback);
    
    gfx = getDefaultDisplay;
    set(h_fig, 'defaultTextInterpreter', gfx.interp);

    set(h_fig, 'defaultFigureColor', gfx.fig_bgcolor);

    set(h_fig, 'defaultAxesFontSize', gfx.fontsize);
    set(h_fig, 'defaultAxesFontName', gfx.fontname);
    set(h_fig, 'defaultAxesFontWeight', gfx.fontweight);
    set(h_fig, 'defaultAxesGridAlpha', 0.3);
    set(h_fig, 'defaultAxesTickLabelInterpreter', gfx.interp);

    set(h_fig, 'defaultColorbarFontSize', gfx.fontsize);
    set(h_fig, 'defaultColorbarFontName', gfx.fontname);
    set(h_fig, 'defaultColorbarFontWeight', gfx.fontweight);
    set(h_fig, 'defaultColorbarTickLabelInterpreter', gfx.interp);

    set(h_fig, 'defaultLegendFontSize', gfx.fontsize);
    set(h_fig, 'defaultLegendFontName', gfx.fontname);
    set(h_fig, 'defaultLegendFontWeight', gfx.fontweight);
    set(h_fig, 'defaultLegendInterpreter', gfx.interp);
    set(h_fig, 'defaultLegendLocation', 'best');

    h_aH = axes('units', 'pixels', 'position', [100, 300, 850, 380]);
    h_lgd = [];
    h_lgd(end+1) = plot(nan, '.', 'color', colors(2,:), 'markersize', 25);
    hold on;
    for i = 1:size(stabColors,1)
        h_lgd(end+1) = plot(nan, 'ws', 'markerfacecolor',...
            stabColors(i,:), 'markersize', 10);
    end
    legend(h_lgd, {'Families', 'SxC', 'CxC', 'MxM', 'SxS', 'SxM'},...
        'location', 'eastoutside', 'autoupdate', 'off');
    xlabel(h_aH, '$\alpha$, deg');
    ylabel(h_aH, '$H_{lt}$, nondim');
    grid(h_aH, 'on');
    set(h_aH, 'fontsize', 12, 'fontweight', 'bold');
    
    loadFamilies;
    updatePlots;
       
    h_viewGroup = uibuttongroup(h_fig, 'units', 'pixels', ...
        'title', 'Views', 'backgroundcolor', 'w',...
        'position', [20, 110, 400, 40], ...
        'SelectionChangedFcn', @viewGroupChanged);
    
    h_viewAlphaH = uicontrol(h_viewGroup, 'style', 'radiobutton', ...
        'string', 'Alpha vs Hlt', 'backgroundcolor', 'w', ...
        'position', [10, 0, 100, 25]);
    
    h_viewPeriodH = uicontrol(h_viewGroup, 'style', 'radiobutton',...
        'string', 'Period vs Hlt', 'backgroundcolor', 'w', ...
        'position', [120, 0, 100, 25]);
    
    h_viewAlphaPeriod = uicontrol(h_viewGroup, 'style', 'radiobutton', ...
        'string', 'Alpha vs Period', 'backgroundcolor', 'w', ...
        'position', [230, 0, 120, 25]);
    
    h_zacGroup = uibuttongroup(h_fig, 'units', 'pixels', ...
        'title', 'ZAC', 'backgroundcolor', 'w',...
        'position', [500, 195, 400, 40], ...
        'SelectionChangedFcn', @zacGroupChanged);
    
    h_viewFam = uicontrol('style', 'pushbutton',...
        'string', 'Add Family to List', ...
        'units', 'pixels', 'position', [500, 160, 200, 25],...
        'callback', @addToListCallback);
    
    h_deleteFam = uicontrol('style', 'pushbutton', ...
        'string', 'Delete Family', ...
        'units', 'pixels', 'position', [710, 160, 100, 25], ...
        'callback', @deleteFamCallback);
    
    h_generateFixA = uicontrol('style', 'pushbutton', ...
        'string', 'Generate: Fixed Angle', ...
        'units', 'pixels', 'position', [500, 120, 175, 25],...
        'callback', @generateFixA);
    
    h_generateFixH = uicontrol('style', 'pushbutton', ...
        'string', 'Generate: Fixed Hlt',...
        'units', 'pixels', 'position', [700, 120, 175, 25],...
        'callback', @generateFixH);
    
    h_generateCenter = uicontrol('style', 'pushbutton', ...
        'string', 'Generate from Center', ...
        'units', 'pixels', 'position', [500, 90, 175, 25],...
        'callback', @generateCenter);
    
    h_zacBtns = [];
    updateZACBtns;
    
    set([h_fig, h_viewGroup, h_viewAlphaH, h_viewPeriodH, h_viewAlphaPeriod,...
        h_aH, h_zacGroup, h_zacBtns, h_viewFam, h_deleteFam,...
        h_generateFixA, h_generateFixH, h_generateCenter],...
        'units', 'normalized');
    
    h_fig.Visible = 'on';
    guiData = struct('h_fig', h_fig, 'h_ax', h_aH, 'updateFcn', @updateFcn);
    
%% Functions
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Callback Functions
    % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    function updateFcn()
        updateZACBtns;
        loadFamilies;
        updatePlots;
    end

    function zacGroupChanged(~, event)
        switch(event.NewValue.String)
            case 'E1'
                zacSelected = 1;
            case 'E2'
                zacSelected = 2;
            case 'E3'
                zacSelected = 3;
            case 'E4'
                zacSelected = 4;
            case 'E5'
                zacSelected = 5;
        end
        
        loadFamilies;
        updatePlots;
    end % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function viewGroupChanged(~, event)
        switch(event.NewValue.String)
            case 'Alpha vs Hlt'
                viewMode = 1;
                xlabel(h_aH, '\alpha, deg');
                ylabel(h_aH, 'H_l_t, nondim');
            case 'Period vs Hlt'
                viewMode = 2;
                xlabel(h_aH, 'Period, nondim');
                ylabel(h_aH, 'H_l_t, nondim');
            case 'Alpha vs Period'
                viewMode = 3;
                xlabel(h_aH, '\alpha, deg');
                ylabel(h_aH, 'Period, nondim');
        end
        updatePlots;
    end

    function [famIx] = selectFam()
        % Select a point
        [a,H] = ginput(1);
        a = a*pi/180;   % Convert deg to rad
        
        lim = axis(h_aH);
        AR = diff(lim(1:2))*pi/180/diff(lim(3:4));
        
        % Find the closest family member to the selected point
        famIx = 0;
        orbIx = 0;
        minDist = inf;
        for k = 1:length(families)
            distVec = [wrapToPi(families{k}.allAlpha) - a, AR*(families{k}.allHlt - H)];
            dist = sum(distVec.*distVec,2);
            [m,i] = min(dist);
            if(m < minDist)
                famIx = k;
                orbIx = i;
                minDist = m;
            end
        end
    end % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function addToListCallback(~, ~)
        famIx = selectFam;
        
        name = sprintf('%s/%s', famFiles(famIx).folder, famFiles(famIx).name);
        
        loadFamCallback(name);  % Use supplied callback to load the family
    end % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function deleteFamCallback(~, ~)
        famIx = selectFam;
        
        answer = questdlg(sprintf('Are you sure you want to delete %s?', ...
            famFiles(famIx).name), 'Just checking!', 'Yes', 'No', 'No');
        
        if(strcmpi(answer, 'yes'))
            name = sprintf('%s/%s', famFiles(famIx).folder, famFiles(famIx).name);
            delete(name);
            fprintf('Deleted %s\n', name);
            
            loadFamilies;
            updatePlots;
        end
    end % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function generateFixA(~, ~)
        opts.Interpreter = 'tex';
        alpha_str = inputdlg('\alpha for family (degrees):', 'Input Alpha', ...
            [1, 40], {'0'}, opts);
        if(isempty(alpha_str)), return; end
        alpha = str2double(alpha_str{1});
        
        % Get a list of the families that have a fixed Hlt value
        candFams = [];
        for i = 1:length(famFiles)
            ix = strfind(famFiles(i).name, 'Hlt');
            if(~isempty(ix))
                candFams(end+1) = i;
            end
        end
        
        % Find all candidate families that have an orbit with about the
        % right alpha value
        tmp = [];
        for i = 1:length(candFams)
            [diff, ~] = min(abs(wrapToPi(families{candFams(i)}.allAlpha) - ...
                wrapToPi(alpha*pi/180)));
            
            if(diff < 5*pi/180)
                tmp(end+1) = candFams(i);
            end
        end
        candFams = tmp;
        
        % Didn't find a suitable family? Throw an error...
        if(isempty(tmp))
            error(['There is not an existing family to seed another family ' ...
               'with alpha = %s deg. Try generating a family from the center '... 
               'subspace of an equilibrium point'], alpha_str{1});
            return;
        else
            famList = cell(length(candFams), 1);
            for j = 1:length(candFams)
                famList{j} = famFiles(candFams(j)).name;
            end
            [indx, ~] = listdlg('PromptString', 'Select an originating family', ...
                'SelectionMode', 'Single', 'ListString', famList, ...
                'listsize', [250, 200]);
            
            if(isempty(indx))
                return;
            else
                famIx = indx;
            end
            fprintf('Initializing from %s\n', famFiles(candFams(famIx)).name);
            
            newFileName = astro_famFixedAlpha(sprintf('%s/%s',...
                famFiles(candFams(famIx)).folder,...
                famFiles(candFams(famIx)).name), alpha*pi/180);
            
            % Once the continuation is done, give the user the opportunity
            % to rename the file
            newFamFile = dir(newFileName);
            if(isempty(newFamFile))
                error('Did not find a new family... check the C++ output');
            else
                % Give user chance to rename file
                newName = inputdlg('Modify the file name:', 'Filename',...
                    [1,40], {newFamFile(1).name});

                % Move the file to the data directory and rename it
                movefile(sprintf('%s/%s', newFamFile(1).folder, ...
                    newFamFile(1).name), sprintf('%s/cr3bp-lt_%s-%s/%s',...
                    dataDir, lower(sharedData.sysParams.P1),...
                    lower(sharedData.sysParams.P2), newName{1}));

                % Update the plots to include the new family
                loadFamilies;
                updatePlots;
            end
        end
    end % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function generateFixH(~,~)
        opts.Interpreter = 'tex';
        H_str = inputdlg('H_l_t for family:', 'Input Hlt', ...
            [1, 40], {'-1.55'}, opts);
        if(isempty(H_str)), return; end
        H = str2double(H_str{1});
        
        % Get a list of the families that have a fixed Hlt value
        candFams = [];
        for i = 1:length(famFiles)
            ix = strfind(famFiles(i).name, 'alph');
            if(~isempty(ix))
                candFams(end+1) = i;
            end
        end
        
        % Find all candidate families that have an orbit with about the
        % right Hlt value
        tmp = [];
        for i = 1:length(candFams)
            [diff, ~] = min(abs(families{candFams(i)}.allHlt - H));
            
            if(diff < 0.005)
                tmp(end+1) = candFams(i);
            end
        end
        candFams = tmp;
        
        % Didn't find a suitable family? Throw an error...
        if(isempty(tmp))
            error(['There is not an existing family to seed another family ' ...
               'with Hlt = %s. Try generating a family from the center '... 
               'subspace of an equilibrium point'], H_str{1});
            return;
        else
            famList = cell(length(candFams), 1);
            for j = 1:length(candFams)
                famList{j} = famFiles(candFams(j)).name;
            end
            [indx, ~] = listdlg('PromptString', 'Select an originating family', ...
                'SelectionMode', 'Single', 'ListString', famList, ...
                'listsize', [250, 200]);
            
            if(isempty(indx))
                return;
            else
                famIx = indx;
            end
            fprintf('Initializing from %s\n', famFiles(candFams(famIx)).name);
            
            newFamName = astro_famFixedHlt(sprintf('%s/%s', ...
                famFiles(candFams(famIx)).folder,...
                famFiles(candFams(famIx)).name), H);
            
            % Once the continuation is done, give the user the opportunity
            % to rename the file
            newFamFile = dir(newFamName);
            if(isempty(newFamFile))
                error('Did not find a new family... check the C++ output');
            else
                % Give user chance to rename file
                newName = inputdlg('Modify the file name:', 'Filename',...
                    [1,40], {newFamFile(1).name});

                % Move the file to the data directory and rename it
                movefile(sprintf('%s/%s', newFamFile(1).folder, ...
                    newFamFile(1).name), sprintf('%s/cr3bp-lt_%s-%s/%s',...
                    dataDir, lower(sharedData.sysParams.P1),...
                    lower(sharedData.sysParams.P2), newName{1}));

                % Update the plots to include the new family
                loadFamilies;
                updatePlots;
            end
        end
    end % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function generateCenter(~, ~)
        % First, select the alpha value
        opts.Interpreter = 'tex';
        alpha_str = inputdlg('\alpha of originating equilibria (degrees):', 'Input Alpha', ...
            [1, 40], {'0'}, opts);
        if(isempty(alpha_str)), return; end
        alpha = str2double(alpha_str{1});
        
        % Next, get all equilibria at the specified alpha value
        data = astro_lowThrustZACs(sharedData.sysParams.P1, ...
            sharedData.sysParams.P2, sharedData.ltParams.fMag);
        [~, pts] = cr3bp_lt_equilib_countDistinct(data, alpha*pi/180, ...
            'zac', zacSelected);
        pts = pts{1};
        
        % Compute stability of each point
        stab = zeros(size(pts,1),1);
        stabIndex = stab;
        for i = 1:size(pts,1)
            [~, ~, ~, ~, Uxx, Uyy, ~, Uxy] = cr3bp_getPseudoPot(...
                sharedData.sysParams.mu3B, [pts(i,2); pts(i,3); 0]);
    
            A = zeros(4);
            A(1:2, 3:4) = eye(2);
            A(3:4, 1:2) = [Uxx, Uxy; Uxy, Uyy];
            A(3,4) = 2;
            A(4,3) = -2;

            [~, v] = eig(A, 'vector');

            pureReal = abs(imag(v(1:4))) < 1e-12;
            pureImag = abs(real(v(1:4))) < 1e-12;
            mixed = ~pureReal & ~pureImag;
            
            if(sum(pureReal) == 2 && sum(pureImag) == 2)
                stab(i) = 1;   % S x C
                stabIndex(i) = max(v(pureReal));
            elseif(sum(pureImag) == 4)
                stab(i) = 2;   % C x C
            elseif(sum(mixed) == 4)
                stab(i) = 3;   % M x M
            elseif(sum(pureReal) == 4)
                stab(i) = 4;   % S x S
                stabIndex(i) = max(v(pureReal));
            elseif(sum(pureReal) == 2 && sum(mixed) == 2)
                stab(i) = 5;   % S x M
                stabIndex(i) = max(v(pureReal));
            else
                warning('unlabeled stability at x = %f, y = %f\n', x(i), y(i));
            end
        end
        
        pt_ix = 1;
        if(size(pts,1) > 1)
            % Need to have user select which point
            % Get a list of the equilibrium points with center modes
            opts_str = {};
            opts_ix = [];
            for i = 1:size(pts,1)
                H = cr3bp_lt_getHamiltonian(sharedData.sysParams.mu3B, [pts(i,2:3),0],...
                    [0,0,0], 1, f, alpha*pi/180, 0);
                if(stab(i) == 1)
                    opts_str{end+1} = sprintf('SxC point at [%.3f, %.3f], Hlt = %.4f',...
                        pts(i,2:3), H);
                    opts_ix(end+1) = i;
                elseif(stab(i) == 2)
                    opts_str{end+1} = sprintf('CxC point at [%.3f, %.3f], Hlt = %.4f',...
                        pts(i,2:3), H);
                    opts_ix(end+1) = i;
                end
            end
            if(isempty(opts_str))
                error(['None of the equilibrium points at alpha = %.2f deg',...
                    ' include a center mode\n'], alpha);
            else
                % User makes the selection
                [ix,~] = listdlg('ListString', opts_str, ...
                    'PromptString', 'Select an equilibrium point:', ...
                    'SelectionMode', 'single', 'listsize', [300, 150]);
                
                % Map the selection to the index of the equilibrium point
                if(isempty(ix))
                    return;
                else
                    pt_ix = opts_ix(ix);
                end
            end
        else
            % Only one option; make sure point has center mode
            if(stab(pt_ix) > 2)
                error(['Equilibrium point at alpha = %.2f deg does not',...
                    ' include a center mode\n'], alpha);
            end
        end
        
        % If we made it this far, a point with a center mode has been
        % selected. Next step: generate the family!
        newFamName = astro_famFromCenter(sharedData.ltParams.fMag,...
            alpha*pi/180, [pts(pt_ix,2:3),0]);
        
        % Once the continuation is done, give the user the opportunity
        % to rename the file
        newFamFile = dir(newFamName);
        if(isempty(newFamFile))
            error('Did not find a new family... check the C++ output');
        else
            % Give user chance to rename file
            newName = inputdlg('Modify the file name:', 'Filename',...
                [1,40], {newFamFile(1).name});

            % Move the file to the data directory and rename it
            movefile(sprintf('%s/%s', newFamFile(1).folder, ...
                newFamFile(1).name), sprintf('%s/cr3bp-lt_%s-%s/%s',...
                dataDir, lower(sharedData.sysParams.P1),...
                lower(sharedData.sysParams.P2), newName{1}));

            % Update the plots to include the new family
            loadFamilies;
            updatePlots;
        end
        
    end % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function updatePlots(~, ~)
        for i = 1:length(h_toDelete)
            delete(h_toDelete{i});
        end
        h_toDelete = {};
        if(viewMode == 1)
            for L = 1:length(sharedData.ltZACs.pos)
                if(L == zacSelected)
                    h_toDelete{end+1} = scatter(h_aH,...
                        (180/pi)*wrapToPi(sharedData.ltZACs.pos{L}(:,1)),...
                        sharedData.ltZACs.Hlt{L}, 8,...
                        sharedData.ltZACs.col_stab(sharedData.ltZACs.stab{L},:),...
                        'filled');
                else
                    h_toDelete{end+1} = scatter(h_aH,...
                        (180/pi)*wrapToPi(sharedData.ltZACs.pos{L}(:,1)),...
                        sharedData.ltZACs.Hlt{L}, 8, 150/200*[1,1,1], 'filled');
                end
            end
        end
        
        if(~isempty(families))
            c = lines(length(families));
            c_hsv = rgb2hsv(c);
            c_hsv(:,2) = 0.5*c_hsv(:,2);
            c = hsv2rgb(c_hsv);

            for i = 1:length(families)
                switch(viewMode)
                    case 1
                        h_toDelete{end+1} = plot(h_aH,...
                            wrapToPi(families{i}.allAlpha)*180/pi,...
                            families{i}.allHlt, '.', 'markersize', 8,...
                            'color', c(i,:));
                    case 2
                        h_toDelete{end+1} = plot(h_aH, families{i}.allT,...
                            families{i}.allHlt, '.', 'markersize', 8,...
                            'color', c(i,:));
                    case 3
                        h_toDelete{end+1} = plot(h_aH, ...
                            wrapToPi(families{i}.allAlpha)*180/pi, ...
                            families{i}.allT, '.', 'markersize', 8,...
                            'color', c(i,:));
                end
            end
        end
    end % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function updateZACBtns()
        if(length(sharedData.ltZACs.pos) ~= length(h_zacBtns))
            for j = 1:length(h_zacBtns)
                delete(h_zacBtns(j));
            end
            h_zacBtns = [];
            
            for j = 1:length(sharedData.ltZACs.pos)
                h_zacBtns(end+1) = uicontrol(h_zacGroup, 'style', 'radiobutton', ...
                    'string', sprintf('E%d', j), 'backgroundcolor', 'w',...
                    'position', [10+80*(j-1), 0, 70, 25]);

                if(j == zacSelected)
                    set(h_zacBtns(end), 'Value', 1);
                end
            end
        end
    end % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    function loadFamilies()
        % First, find all files with the 2112 control law
        famFiles = dir(sprintf('%s/cr3bp-lt_%s-%s/E%d*f%.1e*_law2112.mat', ...
            dataDir, lower(sharedData.sysParams.P1),...
            lower(sharedData.sysParams.P2), zacSelected,...
            sharedData.ltParams.fMag));
        
        clear families;
        families = cell(length(famFiles),1);
        
        for k = 1:length(famFiles)
            families{k} = load(sprintf('%s/%s', famFiles(k).folder, famFiles(k).name));
            famAlpha = zeros(length(families{k}.Members),1);
            famHlt = famAlpha;
            famPeriod = famAlpha;
            
            for j = 1:length(families{k}.Members)
                m = families{k}.Members(j);
                famAlpha(j) = m.NodeCtrlState{1}(1);
                famHlt(j) = cr3bp_lt_getHamiltonian(sharedData.sysParams.mu3B,...
                    m.NodeState(1,1:3), m.NodeState(1,4:6), m.NodeState(1,7),...
                    sharedData.ltParams.fMag, m.NodeCtrlState{1}(1),...
                    m.NodeCtrlState{1}(2));
                famPeriod(j) = sum(m.TOF);
            end
            families{k}.allAlpha = famAlpha;
            families{k}.allHlt = famHlt;
            families{k}.allT = famPeriod;

            clear m;
        end
    end % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end