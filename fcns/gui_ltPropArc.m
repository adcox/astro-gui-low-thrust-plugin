function gui_ltPropArc(sharedData, plotObj, ptIx, infoFcn, propFcn)
    % Use default propagate function
    h_propFig = figure('color', 'w', 'position', [50, 50, 550, 500],...
        'menubar', 'none', 'toolbar', 'none', 'name', 'Low-Thrust Propagate', ...
        'numberTitle', 'off');
    
    str = {'Current point:'};
    switch(plotObj.LawID)
        case 2112
            str{end+1} = sprintf('$\\vec{q}_0 = \\{%.4f, %.4f, %.4f, %.4f, %.4f, %.4f, %.4f\\}$', ...
                plotObj.state(ptIx,1:7));
            str{end+1} = sprintf('$\\alpha = %.2f^{\\circ}, \\beta = %.2f^{\\circ}$',...
                plotObj.ctrl*180/pi);
            str{end+1} = sprintf('$a_{lt} = %.4e$', plotObj.params);
        otherwise
            warning('Law ID = %d is not supported yet');
            close(h_propFig);
            return;
    end
    
    h_qLbl = annotation(h_propFig, 'textbox', 'string', str, 'units', 'pixels',...
        'position', [10, 400, 530, 50], 'backgroundcolor', 'white',...
        'horizontalalignment', 'left', 'interpreter', 'latex',...
        'edgecolor', 'none', 'fontsize', 12);
    
    uicontrol(h_propFig, 'style', 'text', 'string', 'Propagation Ctrl Law:',...
        'units', 'pixels', 'position', [10, 325, 150, 25], ...
        'backgroundcolor', 'w', 'horizontalalignment', 'left');
    
    h_lawPt = uicontrol(h_propFig, 'style', 'popupmenu', 'units', 'pixels',...
        'position', [170, 330, 120, 25],...
        'String', {'General', 'Perp. Vel. (L)', 'Perp. Vel. (R)', ...
        'Along Vel. (+)', 'Along Vel. (-)'}, ...
        'Tooltip', 'Select thrust pointing', 'Value', 1, 'enable', 'off');
    
    h_lawMass = uicontrol(h_propFig, 'style', 'popupmenu', 'units', 'pixels',...
        'position', [300, 330, 120, 25], ...
        'String', {'Const. Mass', 'Var. Mass (CSI)'}, ...
        'Tooltip', 'Select mass variation', 'Value', 1, 'enable', 'off');
    
    h_lawThrust = uicontrol(h_propFig, 'style', 'popupmenu', 'units', 'pixels', ...
        'position', [430, 330, 110, 25], ...
        'String', {'Const. Thrust', 'Var. Thrust'}, ...
        'Tooltip', 'Select thrust variation', 'Value', 1, 'enable', 'off');
    
    h_propParams = uipanel(h_propFig, 'title', 'Propagation Parameters',...
        'units', 'pixels', 'position', [10, 200, 530, 130], ...
        'backgroundcolor', 'w');
    
    uicontrol(h_propParams, 'style', 'text', 'string', 'Alpha (deg):',...
        'units', 'pixels', 'position', [5, 75, 100, 25], ...
        'backgroundcolor', 'w');
    
    h_alphaInput = uicontrol(h_propParams, 'style', 'edit', ...
        'string', sprintf('%.2f', plotObj.ctrl(1)*180/pi), ...
        'units', 'pixels', 'position', [95, 80, 100, 25]);
    
    uicontrol(h_propParams, 'style', 'text', 'string', 'a_lt (nd):', ...
        'units', 'pixels', 'position', [205, 75, 80, 25], ...
        'backgroundcolor', 'w');
    
    h_fMagInput = uicontrol(h_propParams, 'style', 'edit', ...
        'string', sprintf('%.4e', plotObj.params), ...
        'units', 'pixels', 'position', [305, 80, 100, 25]);
    
    uicontrol(h_propParams, 'style', 'text', 'string', 'TOF (nd):',...
        'units', 'pixels', 'position', [5, 45, 80, 25], ...
        'backgroundcolor', 'white');

    h_tofInput = uicontrol(h_propParams, 'style', 'edit', 'string', '0.00', ...
        'units', 'pixels', 'position', [95, 50, 100, 25]);

    h_evt = uipanel(h_propFig, 'Title', 'Events', 'backgroundcolor', 'w', ...
        'units', 'pixels', 'position', [10, 40, 530, 160]);

    uicontrol(h_evt, 'style', 'pushbutton', 'string', '+',...
        'units', 'pixels', 'position', [5, 110, 50, 25], 'enable', 'off');

    uicontrol(h_evt, 'style', 'pushbutton', 'string', '-',...
        'units', 'pixels', 'position', [65, 110, 50, 25], 'enable', 'off');

    uicontrol(h_evt, 'style', 'listbox', 'string', ...
        {'crash'}, 'units', 'pixels',...
        'position', [5, 5, 520, 100], 'enable', 'off');

    uicontrol(h_propFig, 'style', 'pushbutton', ...
        'string', 'Propagate', 'units', 'pixels', ...
        'position', [150, 10, 100, 25], 'callback', @doProp);

    kids = get(h_propFig, 'children');
    set(kids, 'units', 'normalized');
    set(h_qLbl, 'units', 'normalized');
    kids = get(h_propParams, 'children');
    set(kids, 'units', 'normalized');
    kids = get(h_evt, 'children');
    set(kids, 'units', 'normalized');
    
    function doProp(~, ~)
        q0 = plotObj.state(ptIx,1:7);
        tof = str2double(h_tofInput.String);
        alpha = str2double(h_alphaInput.String);
        fMag = str2double(h_fMagInput.String);
        
        revTime = 'off';
        if(tof < 0)
            revTime = 'on';
        end
        
        lawID = 2112;
        
        switch(lawID)
            case 2112
                ctrl0 = [alpha, 0];
                params = fMag;
            otherwise
                warning('Law %d has not been implemented yet', lawID);
        end
        
        arc = cr3bp_lt_simNL(q0, ctrl0, params, lawID,...
            sharedData.sysParams.P1, sharedData.sysParams.P2, 'tof', tof, ...
            'revTime', revTime);
        
        % next step: Add arc to plotted objects
        arcName = inputdlg('Arc name:', 'Filename', [1,40], {'new arc'});
        if(isempty(arcName))
            arcName = {'new arc'};
        end
        
        arc.state = arc.State;
        arc.time = arc.Time;
        arc = rmfield(arc, {'State', 'Time'});
        arc.plotProps = struct('linewidth', 2);
        arc.id = arcName{1};
        arc.name = arcName{1};
        arc.info = infoFcn;
        arc.prop = propFcn;
        
        sharedData.fcns.addPlotObj(arc);
        sharedData.fcns.updatePlot();
        
        close(h_propFig);
    end
end