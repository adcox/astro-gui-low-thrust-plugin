function [ data ] = cr3bp_lt_computeZVC(P1, P2, H, f, alpha, varargin)
%CR3BP_LT_COMPUTEZVC Compute Zero Velocity Curves (ZVCs) at a specified
% hamiltonian value.
%
%	data = cr3bp_lt_computeZVC(P1, P2, H, f, alpha, ...)
%
%   It is assumed that the spacecraft mass is unity.
%
%
%       tol             -   tolerance used in Newton-Raphson methods; 
%                           Default value is 1e-12
%
%       dx              -   the step size in non-dimensional distance units
%                           for stepping horizontally. Default is 5e-4
%
%       dy              -   the step size in non-dimensional distance units
%                           for stepping vertically. Default is 5e-4
%
%       SlopeThresh     -   the slope threshold at which the algorithm
%                           changes from stepping through x to stepping
%                           through y. Increasing this value will cause the
%                           algorithm to step through x on steeper slopes.
%                           The default value is 1.5.
%
%      OverlapTol       -   Overlap detection tolerance. If a
%                           computed coordinate is within this tolerance of
%                           the initial point, the algorithm says the curve has
%                           overlapped itself. Default value is 1.5*(default dy)
%
%       ShowAlgo        -   'On' | 'Off' - Toggles a visualization of the
%                           computation. Default is 'Off'.
%
%       SavePlot        -   'On' | 'Off' - Save the plot from ShowAlgo.
%                           Default is 'Off'.
%
%       Verbose         -   'On' | 'Off' - Toggles some info messages.
%                           Default is 'Off'.
%
%       NicePlots       -   'On' | 'Off' - When On, the algorithm will
%                           insert points of NaN into the data so that the
%                           plot command won't connect points that are far
%                           away, like when the algorithm jumps to a new
%                           IC. Default is 'On'.
%
%
%   Author: Andrew Cox
%   Version: December 19, 2017
%   See: cr3bp_computeZVC
   
%% Algorithm tolerances and settings
mypath = mfilename('fullpath');

% Whether or not to plot with color coded points
def_showAlgo = 'Off';

% Whether or not to save the plot shown with 'ShowAlgo'
def_savePlot = 'Off';

% Whether or not to print out messages
def_verbose = 'Off';

% Whether or not to insert NaN into the point data so that the plot command
% doesn't connect points that shouldn't be
def_nicePlots = 'On';

% Tolerance for Jacobi Constant calculations and Newton Raphson methods
def_tol = 1e-12;

% Step sizes when stepping horizontally (dx) and vertically (dy)
def_dx = 5e-4;
def_dy = 5e-4;

% If the slope exceeds this value, we switch from stepping horizontally to 
%stepping vertically
def_slopeThresh = 1.5;

% Acceptable closeness to say a curve has overlapped
def_overlapTol = 1.5*def_dy;

% Minimum step size; used when dynamically computing the step size in
% Region 4
minStepSize = 1e-6;

% Parse inputs
p = inputParser;

% Function that ensures input is numeric, scalar, and > 0
validationFcn = @(x) isnumeric(x) && isscalar(x);

% Add required and optional inputs
addRequired(p, 'P1', @ischar);
addRequired(p, 'P2', @ischar);
addRequired(p, 'H', validationFcn);
addRequired(p, 'f', validationFcn);
addRequired(p, 'alpha', validationFcn);

addOptional(p,'z',0,@isnumeric);

% Settings the user can set
addParameter(p, 'ShowAlgo', def_showAlgo, @ischar);
addParameter(p, 'SavePlot', def_savePlot, @ischar);
addParameter(p, 'Verbose', def_verbose, @ischar);
addParameter(p, 'NicePlots', def_nicePlots, @ischar);
addParameter(p, 'OverlapTol', def_overlapTol, validationFcn);
addParameter(p, 'SlopeThresh', def_slopeThresh, validationFcn);
addParameter(p, 'dx', def_dx, validationFcn);
addParameter(p, 'dy', def_dy, validationFcn);
addParameter(p, 'tol', def_tol, validationFcn);

% Parse in arguments, apply validation functions
parse(p, P1, P2, H, f, alpha, varargin{:});

% Save users inputs (or defaults if not user specified) to variables
z = p.Results.z;
overlapTol = p.Results.OverlapTol;
slopeThresh = p.Results.SlopeThresh;
dx = p.Results.dx;
dy = p.Results.dy;
tol = p.Results.tol;
showAlgo = strcmpi(p.Results.ShowAlgo, 'On');
verbose = strcmpi(p.Results.Verbose, 'On');
savePlot = strcmpi(p.Results.SavePlot, 'On');
nicePlots = strcmpi(p.Results.NicePlots, 'On');
numIterations = 0;

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computing boundary values and parameters for use in algorithm
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(verbose)
    tic;    % Star the timer
end

% Call C++ executable to compute solutions
binpath = getFullPath(sprintf('%s/../../bin', mypath));
system(sprintf('%s/lowThrustEqPts %s %s %e', binpath, P1, P2, f));
data = load('LPts.mat');
mu = data.Mu;
LPts = cr3bp_getEquilibPts(mu, 'matrix', 'on', 'tol', tol);

saddles = cr3bp_lt_getZACSaddles(mu);

% Get distinct solutions at the desired angle
[~, eqPts] = cr3bp_lt_equilib_countDistinct('LPts.mat', alpha, 'saddles', saddles);

% Compute Hamiltonian at Equilibria
pos = zeros(size(eqPts{1}));
pos(:,1:2) = eqPts{1}(:,2:3);
vel = zeros(size(pos));
alphas = alpha*ones(size(pos,1),1);
betas = zeros(size(alphas));
H_eqPts = cr3bp_lt_getHamiltonian(mu, pos, vel, 1, f, alphas, betas);

% Assign a region based on the energy level. The region is used to pick
% initial guesses for the Newton Raphson processes
region = sum(H >= H_eqPts);
if(region >= size(pos,1))
%     warning('No ZVCs at this energy level (H = %.8f)!', H);
    data = struct('points', [NaN NaN NaN],...
        'numIter', 0,...
        'region', -1,...
        'eqPts', eqPts);
    return;
end

[~,ix] = sort(H_eqPts);
pos = pos(ix,:);
H_eqPts = H_eqPts(ix);

% Compute distances between L1, L2, and the two primaries
r_P1L1 = norm(pos(1,:) - [-mu,0,0]);
r_P2L1 = norm(pos(1,:) - [1-mu,0,0]);
r_P1L2 = norm(pos(2,:) - [-mu,0,0]);
        
% Assign initial guesses based on region
switch(region)
    case 0
        xGuess = [-mu, -mu, 1-mu];
        yGuess = [1.1, 0.8, 0.1];
        desc = 'Region 0: Concentric circles about P1 and a circle around P2';
    case 1
        xGuess = [-mu, -mu];
        yGuess = [1.1*r_P1L2, 1.0*r_P1L1];
        desc = 'Region 1: Horseshoe with L2 closed';
    case 2
		% Try a point along the L3 line with unit distance from the origin
		guess = pos(3,:)/norm(pos(3,:));
		xGuess = guess(1);
		yGuess = guess(2);
        desc = 'Region 2: Horseshoe with L2 open';
    case 3
        r_L4 = pos(4,:) - [-mu,0,0];
        r_L5 = pos(5,:) - [-mu,0,0];
        
        xGuess = -mu + 1.01*[r_L4(1), r_L5(1)];
        yGuess = 1.01*[r_L4(2), r_L5(2)];
        desc = 'Region 3: Tear drops near L4/5';
    case 4
        r_L5 = pos(5,:) - [-mu,0,0];
        xGuess = -mu + 1.01*r_L5(1);
        yGuess = 1.01*r_L5(2);
        
        desc = 'Region 4: Tear drop near L5';
    otherwise
        warning('No ZVCs at this energy level (H = %.8f)!', H);
        data = struct('points', [NaN NaN NaN], 'numIter', 0,...
            'region', -1, 'eqPts', eqPts);
        return;
end

if(region >= 3)
    % Adjust step size based on energy if unless the user has specified
    % something other than the default step size
    if(dx == def_dx)
        % Set the step size to the minimum of two choices: 5e-4 or the
        % greater of 10*(C - C_L4) and 1e-6. This prevents us from
        % using a too tiny step size
        dx = min([5e-4, max(10*abs(H - H_eqPts(5)), minStepSize)]);

        % Also adjust axis crossing tolerance, which is used to detect
        % wrap-arounds in this region
        overlapTol = 1.5*dy;
        if(verbose)
            fprintf('Adjusting dx to %.4e\n', dx);
        end
    end
    % Adjust dy using same logic as above for dx
    if(dy == def_dy)
        dy = min([5e-4, max([10*abs(H - H_eqPts(5)), minStepSize])]);
        overlapTol = 1.5*dy;
        if(verbose)
            fprintf('Adjusting dy to %.4e\n', dy);
        end
    end
end
if(verbose)
    fprintf('Using Region %d ICs\n', region);
    fprintf('%s\n', desc);
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Find points
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ic_idx = 1;         % Which guess we're on
pt_idx = 1;         % Which point we're on
allPts = [];

while(ic_idx <= length(xGuess))
    
    if(region <= 2)
        step = -2;  % Allow algorithm to check both left and right from guess
    else
        step = -3;  % Only check dir = 1 (right, or +x)
    end
    pts = [];
    pt_idx = 1; % Which point we're on
    
    for dir = 1:step:-1
        
        % Find the first point
        x = xGuess(ic_idx);
        [y, iters] = getYVal(x, yGuess(ic_idx), z, H, f, alpha, mu, tol);
        numIterations = numIterations + iters; % Keep count
%         yGuess(ic_idx) = y; % Update initial guess for when we check the other direction
        slope = 0;  % We choose guesses to approximately satisfy this
        pts(pt_idx,:) = [x,y,z,1];
        pt_idx = pt_idx + 1;
        
        % deltaX is the change in x between the past two points. This value
        % allows the algorithm to search around an arc continuously; it can
        % go around corners!
        deltaX = dir;           % Set the initial deltaX to + or - 1
        
        % Set exit condition to false and away we go!
        doneWithArc = false;
        
        while(~doneWithArc)
            if(abs(slope) < slopeThresh)
                % Step in the x-direction
                x = x + dx*sign(deltaX);
                
                % Use Newton-Raphson to search for a new y near our last
                % point (although we've shifted x)
                [y, iters] = getYVal(x, y, z, H, f, alpha, mu, tol);
                
                % Keep track of how many iterations we've done
                numIterations = numIterations + iters;
                
                % Add point to list of points
                pts(pt_idx,:) = [x,y,z,1];
            else
                % Same process as x, just stepping in y direction
                y = y + dy*sign(deltaY);
                [x, iters] = getXVal(x, y, z, H, f, alpha, mu, tol);
                numIterations = numIterations + iters;
                pts(pt_idx,:) = [x,y,z,2];                
            end

            % Compute changes in x and y from the previous point
            deltaX = pts(pt_idx,1) - pts(pt_idx-1,1);
            deltaY = pts(pt_idx,2) - pts(pt_idx-1,2);
            slope = deltaY/deltaX;
            
            pt_idx = pt_idx + 1;
            
            % Kill the process past 50,000 points
            if(pt_idx > 5.0e5)
                break;
            end
            
            % Check exit conditions
            % In this case, the arc should wrap all the way around
            if(pt_idx > 10 && abs(pts(1,1) - x) < overlapTol && ...
                    abs(pts(1,2) - y) < overlapTol)
                doneWithArc = true;
            end
            
            % Absolute exit conditions - limit number of points per arc and
            % direction combo
            if(pt_idx > (75/dx)*ic_idx)
                doneWithArc = true;
            end
        end
        
        if(nicePlots && ~isnan(pts(pt_idx-1,1)))
            % Insert NaN so that plotting won't connect dots that shouldn't 
            % be connected
            pts(pt_idx,:) = [NaN,NaN,NaN,0];
            pt_idx = pt_idx + 1;
        end
    
    end% End of dir. loop; either change dir. or move to next guess
    
    % Try the next guess!
    ic_idx = ic_idx + 1;
    
    if(nicePlots && ~isnan(pts(pt_idx-1,1)))
        % Insert NaN so that plotting won't connect dots that shouldn't be
        % connected
        pts(pt_idx,:) = [NaN,NaN,NaN,0];
        pt_idx = pt_idx + 1;
    end
    
    allPts = [allPts; pts];
end


%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Outputs
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if(verbose)
    toc;
    fprintf('Used %d points\n', pt_idx);
    fprintf('Used %ld Newton-Raphson iterations\n', numIterations);
    fprintf('\tor %.2f iterations per point\n', numIterations/pt_idx);
end

if(showAlgo)
    gray = 200/255*ones(1,3);
    gfx = getDefaultDisplay();
    
    fig = figure();
    hold on;

    i_stepX = allPts(:,4) == 1;
    redHandle = plot(allPts(i_stepX,1), allPts(i_stepX,2), '.r', 'markersize', 8);
    blueHandle = plot(allPts(~i_stepX,1), allPts(~i_stepX,2), '.b', 'markersize', 8);
    guessHandle = plot(xGuess, yGuess, 'og', 'MarkerSize', 7.5, 'MarkerFaceColor', 'g');
    plot(LPts(:,1), LPts(:,2), 'd', 'color', gray, 'markerfacecolor', gray);
    plot(eqPts{1}(:,2), eqPts{1}(:,3), 'k*');
    plot(-mu,0,'o', 'MarkerSize', 10, 'color', gray, 'markerfacecolor', gray);
    plot(1-mu,0, 'o', 'MarkerSize', 7, 'color', gray, 'markerfacecolor', gray);
    legend([redHandle, blueHandle, guessHandle], ...
        'Horiz. Stepping', 'Vert. Stepping', 'Initial Guesses');
    hold off; grid on; axis equal;
    xlabel('x, non-dim');
    ylabel('y, non-dim');
    setFigProps(fig, gfx);
end

% Save data
% Set Region to 5 so that no symmetry is assumed
data = struct('points', allPts, 'numIter', numIterations,...
    'region', region, 'eqPts', eqPts);
end

function [x,count] = getXVal(xGuess, y, z, H, f, alpha, mu, tol)
maxIt = 50;         % Maximum Allowable number of iterations
count = 0;          % Iteration counter

prevX = -999;
x = xGuess;
while(abs(x - prevX) > tol && count < maxIt)
    prevX = x;
    d = sqrt((x + mu)^2 + y^2 + z^2);
    r = sqrt((x + mu - 1)^2 + y^2 + z^2);
    U = (1-mu)/d + mu/r + 0.5*(x^2 + y^2);
    H1 = -U - f * (x*cos(alpha) + y*sin(alpha));
    
    dF_dx = (1-mu)*(x + mu)/d^3 + mu*(x + mu -1 )/r^3 - x - f*cos(alpha);
    F = H1 - H;
    
    x = prevX - F/dF_dx;
    count = count + 1;
end

if(abs(x - prevX) > tol)
    warning('getXVal did not converge');
end

end



function [y,count] = getYVal(x, yGuess, z, H, f, alpha, mu, tol)

maxIt = 50;         % Maximum Allowable number of iterations
count = 0;          % Iteration counter

prevY = -999;
y = yGuess;
while(abs(y - prevY) > tol && count < maxIt)
    prevY = y;
    d = sqrt((x + mu)^2 + y^2 + z^2);
    r = sqrt((x + mu - 1)^2 + y^2 + z^2);
    U = (1-mu)/d + mu/r + 0.5*(x^2 + y^2);
    H1 = -U - f * (x*cos(alpha) + y*sin(alpha));
    
    F = H1 - H;
    dF_dy = (1-mu)*y/d^3 + mu*y/r^3 - y - f*sin(alpha);
        
    y = prevY - F/dF_dy;
    count = count + 1;
end

if(abs(y - prevY) > tol)
    warning('getYVal did not converge');
end

end


