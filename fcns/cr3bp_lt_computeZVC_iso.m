function points = cr3bp_lt_computeZVC_iso(P1, P2, Hlt, fMag, alpha)
% CR3BP_LT_COMPUTEZVC_ISO Compute the low-thrust ZVCs using an isocontour
%
%   points = cr3bp_lt_computeZVC_iso(P1, P2, Hlt, fMag, alpha) computes the
%   low-thrust ZVC for the P1-P2 CR3BP-LT system with thrust magnitude fMag
%   (nondim) and thrust orientation angle alpha (rad). The output is an nx3
%   array of points (x, y, z) that lie on the ZVC
%
%   Author: Andrew Cox
%   Version: September 24, 2018

    % Get the CR3BP mass parameter
    mu3B = cr3bp_getSysParam(P1, P2);

    % Create a grid of points in the x-y plane
    xGrid = linspace(-1.5, 1.5, 300);
    yGrid = xGrid;
    [X, Y] = meshgrid(xGrid, yGrid);
    
    % Compute the low-thrust Hamiltonian at all points in the plane
    r1 = sqrt((X + mu3B).^2 + Y.^2);
    r2 = sqrt((X + mu3B - 1).^2 + Y.^2);
    
    a_lt = fMag*[cos(alpha); sin(alpha)];
    HLT = -0.5*(X.^2 + Y.^2) - (1-mu3B)./r1 - mu3B./r2;
    
    % Do the dot product in a loop
    for c = 1:size(Y,1)
        HLT(:,c) = HLT(:,c) - [X(:,c), Y(:,c)]*a_lt;
    end

    % Use contourc to compute a contour of points at the desired low-thrust
    % Hamiltonian value. The first column contains [Hlt; numPts] where Hlt
    % is the Hamiltonian value of the contour, and numPts is the number of
    % points on that contour. If the ZVC has multiple contours (e.g., the
    % L4/5 islands), there is an additional column that lists the number of
    % points on that arc.
    C = contourc(xGrid, yGrid, HLT, [Hlt, Hlt]);
    
    % Delete or replace the bookkeeping columns with NaN values to make
    % plotting easier and get rid of pesky connecting lines
    ix = 1;
    while(ix < size(C,2))
        nextIx = ix + C(2,ix) + 1;
        if(ix > 1)
            C(:,ix) = NaN(2,1);
        else
            C(:,ix) = [];
            nextIx = nextIx - 1;
        end
        ix = nextIx;
    end
    
    % Convert to rows, add zeros for z
    points = [C', zeros(size(C,2),1)];
end