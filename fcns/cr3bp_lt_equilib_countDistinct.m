function [numPtCounts, allUniquePts] = cr3bp_lt_equilib_countDistinct(...
    data, alphaGrid, varargin)
%% CR3BP_LT_EQUILIB_COUNTDISTINCT Compute the distinct equilibria over a 
%   range of thrust pointing angles.
%
%   count = cr3bp_lt_equilib_countDistinct(data, alphaGrid, ...)
%   computes the distinct equilbria using the data stored `data`
%   over the range of angles in `alphaGrid`. The output,
%   `count`, is the same size as alphaGrid and gives the number of distinct
%   equilibria at each angle in alphaGrid.
%
%   The data in `data` is obtained from the mexed function
%   'astro_lowThrustZACs'
%
%   [count, pts] = cr3bp_lt_equilib_countDistinct(data, alphaGrid, ...)
%   also returns the distinct points in the cell array `pts`. Each cell
%   corresponds to the angle in `alphaGrid` at the same index. The data in
%   the cell is stored in row-major format, with the columns containing the
%   thrust angle, alpha, in radians, and the (x,y) location of the
%   equilibria in nondimensional coordinates, e.g.,
%
%   pts{1} = [alpha1, x1, y1; alpha2, x2, y2; ... ]
%
%   Optional inputs:
%
%       'zac'           -   Enter index of a ZAC to count the equilibria
%                           on. By default, the value is 0, indicating all
%                           ZACs are searched to locate distinct points. If
%                           a value of 1 is used, only the E1 ZAC is
%                           searched for distinct points.
%       'makeplots'     -   logical (true/false) value that tells the
%                           function whether or not to plot the results.
%       'saddles'       -   A 6x4 array in which each row contains the
%                           [mu, f, x, y] data of a saddle in the
%                           acceleration magnitude field. The locations of
%                           the saddles in {f, x, y} space are the
%                           boundaries between equilibria count changes.
%                           Saddles are computed by the matlab function,
%                           cr3bp_lt_getZACSaddles()
%
%   See: cr3bp_lt_getZACSaddles
%   Author: Andrew Cox
%   Version: December 15, 2017

    %% Parse Inputs
    p = inputParser;
    
    addRequired(p, 'data');
    addRequired(p, 'alphaGrid', @isnumeric);
    
    addParameter(p, 'makeplots', false, @islogical);
    addParameter(p, 'saddles', [], @isnumeric);
    addParameter(p, 'zac', 0, @isnumeric);
    
    parse(p, data, alphaGrid, varargin{:});
    
    makeplots = p.Results.makeplots;
    saddles = p.Results.saddles;
    zacSelected = p.Results.zac;
    
    %% Analysis
    
    % Load the data, initialize storage vectors
    numPtCounts = zeros(length(alphaGrid), 1);
    allUniquePts = cell(length(alphaGrid), 1);
    
    % Compute the locations of the saddles if they haven't been passed in
    if(isempty(saddles))
        saddles = cr3bp_lt_getZACSaddles(data.Mu);  % Get saddles
    end
    
    [~,ix] = sort(saddles(:,2));    % Sort by thrust value
    saddles = saddles(ix,:);
    
    % The saddles bound the number of distinct ZAC structures
    if(data.fMag < saddles(1,2))
        % All give equilibrium structures are separate
        L_grid = 1:5;
        
        % Construct the L5 solutions
        op = [-1;1;-1]*ones(1,size(data.EqPts{4},2));
        data.EqPts{5} = data.EqPts{4}.*op;
    else
        % L3 has merged with L4/L5, so there are only three distinct
        % structures left
        L_grid = 1:3;
    end
    
    if(zacSelected > 0)
        if(zacSelected >= min(L_grid) && zacSelected <= max(L_grid))
            L_grid = zacSelected;
        else
            error('zac = %d is invalid; must be between %d and %d\n', ...
                zacSelected, min(L_grid), max(L_grid));
        end
    end
    % Clean up data
    for ix_L = 1:length(L_grid)
        data.EqPts{L_grid(ix_L)}(:,1) = wrapToPi(data.EqPts{L_grid(ix_L)}(:,1), pi);
    end
    
    for ix_a = 1:length(alphaGrid)
        pts = [];   % Storage for all points that *may* be distinct
        all_dX = zeros(length(L_grid),1);   % Store variation in x-coordinates 
        all_dY = zeros(length(L_grid),1);   % Store variation in y-coordinates
        
        % Find all the points that exist at the specified angle across all
        % the ZAC structures
        for ix_L = 1:length(L_grid)
            pts_exact = [];
            
            % Compute the difference in sin(alpha) and cos(alpha) between
            % datasets. This avoids difficulties with angles that are
            % equivalent but different numerically (e.g., 0 and 2*pi)
            diffS = sin(alphaGrid(ix_a)) - sin(data.EqPts{L_grid(ix_L)}(:,1));
            diffC = cos(alphaGrid(ix_a)) - cos(data.EqPts{L_grid(ix_L)}(:,1));
            
            % First, locate any exact zeros (to numerical precision)
            exactS = abs(diffS) < 1e-12;
            exactC = abs(diffC) < 1e-12;
            pts_exact = data.EqPts{L_grid(ix_L)}(exactS & exactC, :);
            
            % Next, locate all points that occur before a sign change in
            % diffS or diffC; this means diffS or diffC has crossed zero!
            dsignS = diffS(1:end-1).*diffS(2:end) < 0;
            dsignC = diffC(1:end-1).*diffC(2:end) < 0;

            % If we haven't found any crossings or tangential approaches, 
            % try to find zeros with wider net
            if(isempty(pts_exact) && sum(dsignS) == 0)
                dsignS = abs(diffS(1:end-1)) < 1e-4;
            end
            if(isempty(pts_exact) && sum(dsignC) == 0)
                dsignC = abs(diffC(1:end-1)) < 1e-4;
            end
            
            % Locate points where both the sine and cosine comparisons
            % cross zero; both must be true for the angles to be guaranteed
            % to be identical (quadrant check)
            ix_match = find(dsignS & dsignC);

            % Use linear interpolation to find exact zeros by looking at
            % the points *after* the ones identified above
            for ix_m = 1:length(ix_match)
                % Use linear approximation to locate where diffS line passes
                % through diffS = 0
                i = ix_match(ix_m);
                p = -diffS(i)/(diffS(i+1) - diffS(i));  % Percentage between i and i+1 that zero occurs
                if(isnan(p) || isinf(p)), p = 0.5; end  % Two diffS values are identical, so exact average
                
                p1 = data.EqPts{L_grid(ix_L)}(i,:);
                p2 = data.EqPts{L_grid(ix_L)}(i+1,:);
                % Use sin(alpha) and cos(alpha) in the linear interpolation
                pt1 = [sin(p1(1)), cos(p1(1)), p1(2:3)];
                pt2 = [sin(p2(1)), cos(p2(1)), p2(2:3)];
                pa = pt1 + p*(pt2 - pt1);
                % Back-out angle from arccos and arcsin options
                angle = commonAngle([acosd(pa(2)), -acosd(pa(2)), asind(pa(1)), 180 - asind(pa(1))]);
                % Save the averaged point
                pts_exact(end+1,:) = [angle*pi/180, pa(3:4)];
            end

        %   Use information from the distribution of points in the ZAC data to
        %   determine appropriate numerical bounds
            diffX = abs(data.EqPts{L_grid(ix_L)}(1:end-1,2) - data.EqPts{L_grid(ix_L)}(2:end,2));
            diffY = abs(data.EqPts{L_grid(ix_L)}(1:end-1,3) - data.EqPts{L_grid(ix_L)}(2:end,3));
        %   Get statistics
            avgDX = mean(diffX);
            stdDX = std(diffX);
            avgDY = mean(diffY);
            stdDY = std(diffY);

            all_dX(ix_L) = avgDX + 3*stdDX;
            all_dY(ix_L) = avgDY + 3*stdDY;
            pts = [pts; pts_exact];   % Save average points to the total list
        end % END OF LOOP THROUGH L_GRID

        % Average the acceptable spread distances from all the structures to
        % determine if any of the points from the different structures overlap
        dx = mean(all_dX);
        dy = mean(all_dY);

        % Look through all the points that match the angle across all the
        % ZAC structures and keep the ones that are unique.
        uniquePts = [];
        for ix_p = 1:size(pts,1)
            if(ix_p == 1)
                uniquePts = pts(ix_p,:);    % Always keep the first one
            else
                % Check to see if subsequent points are within dx and dy of
                % other unique points. Recall: dx and dy are the averaged
                % 3-sigma spreads of the x- and y-variations across all
                % points in all ZAC structures
                isUnique = true;
                for ix_u = 1:size(uniquePts,1)
                    if( abs(uniquePts(ix_u,2) - pts(ix_p,2)) < dx &&...
                            abs(uniquePts(ix_u,3) - pts(ix_p,3)) < dy )
                        isUnique = false;
                        break;
                    end
                end

                if(isUnique)
                    uniquePts = [uniquePts; pts(ix_p,:)];
                end
            end
        end

        % Save the unique points and count for this angle
        allUniquePts{ix_a} = uniquePts;
        numPtCounts(ix_a) = min(5, size(uniquePts,1));
    end % END OF LOOP THROUGH ALPHAGRID
    
    % Make plots
    if(makeplots)
        gfx = getDefaultDisplay();
        
        figure();
        subplot(7,1,1:3); hold on;
        for ix_L = 1:length(data.EqPts)
            plot(wrapToPi(data.EqPts{ix_L}(:,1), pi), data.EqPts{ix_L}(:,2), '.', 'markersize', 12);
        end
        hold off; grid on;
        set(gca, 'xlim', [min(alphaGrid), max(alphaGrid)]);
        ylabel('x, nondim');

        subplot(7,1,4:6); hold on;
        for ix_L = 1:length(data.EqPts)
            plot(wrapToPi(data.EqPts{ix_L}(:,1), pi), data.EqPts{ix_L}(:,3), '.', 'markersize', 12);
        end
        set(gca, 'xlim', [min(alphaGrid), max(alphaGrid)]);
        hold off; grid on;
        ylabel('y, nondim');

        subplot(7,1,7);
        stairs(alphaGrid, numPtCounts, 'linewidth', 2);
        xlabel('\alpha, rad');
        ylabel('Count');
        grid on;
        set(gca, 'xlim', [min(alphaGrid), max(alphaGrid)]);
        setFigProps(gcf, gfx);
    end
end