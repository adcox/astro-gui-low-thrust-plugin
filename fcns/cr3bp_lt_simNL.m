function [ output ] = cr3bp_lt_simNL( q0, ctrl0, params, LawID, P1, P2, varargin)
% CR3BP_LT_SIMNL Simulate the non-linear system
%
%   data = cr3bp_lt_simNL(q0, ctrl0, params, lawID, P1, P2, ...) takes the 
%       initial state state (q0 = [x y z vx vy vz, m]  at t0), low-thrust 
%       engine parameters, a control law ID, and the names of two primaries
%       as inputs. The function then propogates the simulation 
%       through one rotation of the primaries and outputs a structure that 
%       contains a variety of information about the system. See the 
%       OUTPUTS description below.
%
%       State inputs should be in non-dimensional units.
%           i.e., position and velocity are nondimensionalized by the
%           characteristic length and time, mass is nondimensionalized by
%           the initial spacecraft mass (M0)
%
%       Low-thrust inputs:
%           ctrl0   -   Vector of control state variables
%           params  -   Vector of constant control parameters
%           LawID   -   Control law ID: a 16-bit unsigned integer that
%                       stores information about the control law in several
%                       multi-bit registers
%
%   You can specify several options after the required parameters in
%   tag-value pairs (case-insensitive):
%
%       'tof'       -   Simulation time duration (units specified by 
%                       tfUnit). Default value is 2*pi
%
%       't0'        -   Initial time for the propagation
%
%       'tfUnit'    -   Specify unit for tf. Options are:
%                           'nd'    -   Non-Dimensional Time (Default)
%                           'dim'   -   Dimensional time in seconds
%                           'rot'   -   Primary Rotations (2*pi nd units)
%
%       'mu'        -   Specify a custom value for mu. This will override
%                       the mass ratio from P1 and P2 without changing the
%                       characteristic mass, length, or time.
%
%       'revTime'   -   'On' | 'Off' - To run a simulation in reverse time,
%                       set this setting to 'On'. Default is 'Off'. The
%                       value of tf should be positive regardless of this
%                       setting.
%
%       'exitCond'  -   Specify an exit condition. Options are:
%                           'mass'      -   Exit when mass hits 75% of the
%                                           initial spacecraft mass
%                                           (Default)
%                           'xzCross'   -   Exit at tf or the first
%                                           xz-plane crossing
%                           'xzCross+'  -   Exit at tf or the first xz-plane
%                                           crossing where y-dot is
%                                           positive
%                           'xzCross-'  -   Exit at tf or the first xz-plane
%                                           crossing where y-dot is
%                                           negative
%                           'xyCross'   -   Exit at tf or the first
%                                           xy-plane crossing
%                           'xyCross+'  -   Exit at tf or the first
%                                           xy-plane crossing where z-dot
%                                           is positive
%                           'xyCross-'  -   Exit at tf or the first
%                                           xy-plane crossing where z-dot
%                                           is negative
%                           'crash'     -   Exit at tf or when P3 crashes
%                                           into P1 or P2
%                           'none'      -   Integrate to tf and don't check
%                                           for crashes or axis crossings
%                           'custom'    -   Provide a function handle for
%                                           your own exit function. See the
%                                           documentation for EventFunc.
%
%       'EventFunc' -   Provide a handle to your own Events function. This
%                       function must take time t and state y as inputs;
%                       for example, your handle could take the form:
%
%                           funcHandle = @(t,y)MyEventFunction(t, y, a, b)
%
%                       For information about creating an events function,
%                       see the ODE documentation online
%
%       'Tol'       -   Set the integration tolerance; also applies to ZVC
%                       generation and Lagrange point computation. Default
%                       value is 1e-12
%
%       'Timeout'   -   Set a limit on how long the integrator can run;
%                       argument should be in seconds. By default, no
%                       timeout is enforced
%
%       'Verbose'   -   'On' | 'Off' Set whether the program writes verbose
%                       outputs to the screen
%
%       'NumPts'    -   Set the number of points the integrator should
%                       return. By default, NumPts is set to zero and the
%                       integrator returns the steps distributed unevenly
%                       in time according to the integration stepping
%                       strategy. A nonzero NumPts value calls the deval()
%                       function to resample the propagated trajectory with
%                       even time steps between the initial and final
%                       times. This may help smooth out arcs when the
%                       integrator takes large steps.
%
%   See also ODE113
%
%   Author: Andrew Cox
%   Version: March 30, 2017

% Constants not negotiable by user
defineConstants;

%% Algorithm defaults, settings, tolerances

% Default simulation length, non-dim time
def_tof = 2*pi;

% Default initial time, nondim
def_t0 = 0;

% Unit for tf - see comments above
def_tfUnit = 'nd';

% Default mu value; EM System
def_mu = 0.012150586550569;

% Whether or not to run the simulation in reverse time
def_revTime = 'Off';

% Default exit condition - see comments above
def_exitCond = 'mass';

% Default integration tolerance; also applies to ZVC generation, Lagrange
% point computation, etc.
def_tol = 1e-12;

% Whether or not to print out verbose outputs
def_verbose = 'Off';

% Default timeout value
def_timeout = NaN;

% Default custom exit function handle
def_eventFunc = [];

def_pts = 0;

%% Parse inputs
p = inputParser;

% Validation functions
stateValidFcn = @(x) isnumeric(x) && length(x) == 7;
posNumFcn = @(x) isnumeric(x) && isscalar(x) && x > 0;
validMu = @(x) isnumeric(x) && isscalar(x) && x >= 0 && x <= 0.5;

% Add required and optional inputs
addRequired(p,'q0', stateValidFcn);
addRequired(p, 'ctrl0', @isnumeric);
addRequired(p, 'params', @isnumeric);
addRequired(p, 'LawID', @isnumeric);
addRequired(p, 'P1', @ischar);
addRequired(p, 'P2', @ischar);

% Optional settings
addParameter(p, 'Tol', def_tol, posNumFcn);

% Optional parameters
addParameter(p, 'tf', def_tof, @isnumeric);
addParameter(p, 'tof', def_tof, @isnumeric);
addParameter(p, 't0', def_t0, @isnumeric);
addParameter(p, 'tfUnit', def_tfUnit, @ischar);
addParameter(p, 'mu', def_mu, validMu);
addParameter(p, 'exitCond', def_exitCond, @ischar);
addParameter(p, 'EventFunc', def_eventFunc);
addParameter(p, 'Verbose', def_verbose, @ischar);
addParameter(p, 'RevTime', def_revTime, @ischar);
addParameter(p, 'Timeout', def_timeout);
addParameter(p, 'NumPts', def_pts);

% Parse in arguments, apply validation functions
parse(p, q0, ctrl0, params, LawID, P1, P2, varargin{:});

P1_dat = getBodyParameters(P1);
P2_dat = getBodyParameters(P2);

% Settings
tol = p.Results.Tol;
maxRunTime = p.Results.Timeout;
numPts = p.Results.NumPts;

% Options
verbose = strcmpi(p.Results.Verbose, 'On');
revTime = strcmpi(p.Results.RevTime, 'On');

%% Computations

% Compute characteristic quantities
charL = P2_dat.a;                               % Characteristic length, km

if(sum(ismember(lower(p.UsingDefaults), 'mu')) == 0)
    mu3BP = p.Results.mu;
    
    % Adjust Mass of P2 to fit mass ratio without changing mass of P1
    p2Mass = P1_dat.mass/(1-1/mu3BP);
    charM = P1_dat.mass + p2Mass;
else
    charM = P1_dat.mass + P2_dat.mass;              % Characteristic mass, kg
    mu3BP = P2_dat.mass/charM;                      % System mass ratio, nd
end

charT = sqrt(charL^3 / (G*charM));                  % Characteristic time, s

% Compute the ending time
tfUnit = lower(p.Results.tfUnit);
t0 = p.Results.t0;
tof = p.Results.tof;
if(tof == def_tof && p.Results.tf ~= def_tof)
    tof = p.Results.tf;
end

if(tof < 0 && ~revTime)
    warning('tof is negative, but we are not using reverse time; results may not be what you want!');
end

if(revTime)
    tof = -abs(tof);
else
    tof = abs(tof);
end

switch(tfUnit)
    case 'nd'
        tf = t0 + tof;
    case 'dim'
        tf = t0 + tof/charT;
    case 'rot'
        tf = t0 + tof*2*pi;
    otherwise
        error('Unknown tfUnit %s\n', tfUnit);
end

% Nondimensional primary radii
R1 = P1_dat.radius/charL;
R2 = P2_dat.radius/charL;

% Transpose q0 if it is vertical
if(size(q0,1) ~= 1)
    q0 = q0.';
end

I = eye(7);     % initial state for STM
initState = [q0, reshape(I,1,7*7)];

% Create a function handle for the crash events function
exitCond = lower(p.Results.exitCond);
switch exitCond
    case 'mass'
        event = @(t,y)cr3bp_lt_event_mass(t,y,0.75);
    case 'xzcross'
        event = @(t,y)cr3bp_event_crossXZ(t,y,0);
    case 'xzcross+'
        event = @(t,y)cr3bp_event_crossXZ(t,y,1);
    case 'xzcross-'
        event = @(t,y)cr3bp_event_crossXZ(t,y,-1);
    case 'xycross'
        event = @(t,y)cr3bp_event_crossXY(t,y,0);
    case 'xycross+'
        event = @(t,y)cr3bp_event_crossXY(t,y,1);
    case 'xycross-'
        event = @(t,y)cr3bp_event_crossXY(t,y,-1);
    case 'crash'
        event = @(t,y)cr3bp_event_crash(t,y,mu3BP,R1,R2);
    case 'none'
        event = [];
    case 'custom'
        event = p.Results.EventFunc;
        if(isempty(event))
            warning('Custom exit function selected but not provided!\n');
        end
    otherwise
        error('Unknown exit condition: %s\n', exitCond);
end

% Use an output function to limit run time
if(~isnan(maxRunTime))
    tStart = tic;
    outputFcn = @(t,y,flag)odeOutFcn_LimitRunTime(t, y, flag, tStart, maxRunTime);
else
    outputFcn = [];
end

% Parse the single 16-bit ID into its sub-types
lawIDs = parseLTID(LawID);

% Set ODE options
options = odeset('RelTol', tol, 'AbsTol', tol, 'Events', event, ...
    'OutputFcn', outputFcn);
    
eoms = @(t,y)cr3bp_lt_EOMs(t, y, mu3BP, lawIDs, ctrl0, params, charT, charL);

t = [t0, tf];

% Perform integration
data = ode113(eoms, t, initState, options);

if(isempty(event) && data.x(end) ~= t(end))
    error('Integration did not complete; probably timed out');
end

if(numPts > 0)
    t = (linspace(t0, tf, numPts)).';
    sol = (deval(data, t)).';
else
    sol = data.y.';
    t = data.x.';
end

STM = zeros(7,7, length(t));
for i = 1:length(t)
    STM(:,:,i) = reshape(sol(i,8:end),7,7).';
end


state = sol(:,1:7);
C = cr3bp_jacobiAt(sol(:,1:3), sol(:,4:6), mu3BP);
H = [];
Accel = nan(length(t), 3);

% Clear up some memory
clear s sol;

%% Outputs

if(verbose)
    fprintf('********\nsimNL Outputs\n********\n');
    fprintf('Total Integration Time: %.8f = %.6f rots = %.4f days\n',...
        t(end), t(end)/(2*pi), t(end)*charT/(3600*24));
    fprintf('# Data Points: %d\n', length(t));
    fprintf('Initial Position:\n\t[%.4f, %.4f, %.4f]\n\t[%.4f, %.4f, %.4f] km\n',...
            q0(1:3), q0(1:3)*charL);

    fprintf('Initial Velocity:\n\t[%.4f, %.4f, %.4f]\n\t[%.4f, %.4f, %.4f] km/s\n',...
        q0(4:6), q0(4:6)*charL/charT);
    
    fprintf('Final Position:\n\t[%.4f, %.4f, %.4f]\n\t[%.4f, %.4f, %.4f] km\n',...
        state(end,1:3), state(end,1:3)*charL);
    
    fprintf('Final Velocity:\n\t[%.4f, %.4f, %.4f]\n\t[%.4f, %.4f, %.4f] km\n',...
        state(end,4:6), state(end,4:6)*charL);
    
    fprintf('Jacobi: %.8f\n', C(1));
    fprintf('\tAvg Error: %.4e\n', mean(C - C(1)));
end

% Put default info into the data structure
output = struct('Time', t, 'State', state, 'Accel', Accel, 'STM', STM,...
    'Jacobi', C, 'H_lt', H, 'P1', P1, 'P2', P2, 'Mu', mu3BP, 'LawID', LawID,...
    'ctrl', ctrl0, 'params', params, 'odeData', data);

end % END OF FUNCTION