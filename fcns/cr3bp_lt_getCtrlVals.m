function [f, u, mdot] = cr3bp_lt_getCtrlVals(t, s, ctrl, params, lawIDs, charT, charL)
% CR3BP_LT_GETCTRLVALS Compute control values from the state and lawIDs
%
%   [f, u, mdot] = cr3bp_lt_getCtrlVals(t, s, ctrl, params, lawIDs, charT, charL) 
%   uses the inputs,
%
%       t       -   Current time (nondim)
%       s       -   Current full state vector (nondim)
%       ctrl    -   Vector of control variables (nondim)
%       params  -   Vector of constant control parameters (nondim)
%       lawIDs  -   Parsed values from the 16-bit binary ID
%       charT   -   CR3BP characteristic time, sec
%       charL   -   CR3BP characteristic length, km
%
%   Author: Andrew Cox
%   Version: April 6, 2018
G_GRAV = 9.80665/1000;    % Acceleration, km/s^2

% If ID = NO_CTRL, everything is zero
if(sum(abs(lawIDs)) == 0)
    u = [0;0;0];
    f = 0;
    mdot = 0;
    return;
end

% Thrust Pointing
switch(lawIDs(1))
    case 1  % General Pointing
        u = [cos(ctrl(1))*cos(ctrl(2)); 
            sin(ctrl(1))*cos(ctrl(2));
            sin(ctrl(2))];
    case 2  % Velocity Pointing
        u = (-1)^lawIDs(4) * s(4:6)/norm(s(4:6));
    case 3  % Jacobi-Preserving
        u = (-1)^lawIDs(4) * [-s(5); s(4); 0]/norm(s(4:5));
    otherwise
        error('Unrecognized pointing parameterization ID');
end

switch(lawIDs(2))
    case 0  % Constant Thrust
        f = params(1);
    case 1  % Variable thrust, f = fmax/2*(sin(g) + 1)
       if(lawIDs(1) == 1)
           g = ctrl(3);
       else
           g = ctrl(1);
       end
       f = 0.5*params(1)*(sin(g) + 1);
    case 2  % Variable thrust, f = 10^(4g)
        if(lawIDs(1) == 1)
           g = ctrl(3);
       else
           g = ctrl(1);
        end
       f = params(1)*g^2;
    otherwise
        error('Unrecognized thrust parameterization ID');
end

switch(lawIDs(3))
    case 1
        mdot = 0;
    case 0
        if(lawIDs(2) == 0)
            Isp = params(2);
        else
            Isp = params(1);
        end
        mdot = -charL*f/(charT*Isp*G_GRAV);
    otherwise
        error('Unrecognized mass parameterization ID');
end

end