function gui_ltEqPtPropFig(sharedData, plotObj, ptIx, eigVec, eigVal,...
    arcInfoFcn, arcPropFcn)
% GUI_LTEQPTPROPFIG Creates a figure to take user inputs to propagate an
% arc from a low-thrust equilibrium point
%
%   gui_ltEqPtPropFig(data, plotObj, ptIx, eigVec, eigVal, arcInfoFcn,
%   arcPropFcn)
%
%   Inputs:
%
%       data        - shared data object from the core interface
%       plotObj     - the plot object that has been selected for 
%                   propagation; this should be the low-thrust equilibrium
%                   point object
%       ptIx        - index of the data point within plotObj that is
%                   closest to the pointer when the propagate button was
%                   clicked
%       eigVec      - 6x6 matrix of eigenvectors associated with the
%                   equilibrium point; each column is an eigenvector
%       eigVal      - 6x1 vector of eigenvalues, each associated with the
%                   corresponding column of the eigenvector matrix
%       arcInfoFcn  - function pointer to an info function for the
%                   propagated arc
%       arcPropFcn  - function pointer to a propagation function for the
%                   propagated arc
%
%   Author: Andrew Cox
%   Version: September 18, 2018

    % Use default propagate function
    h_propFig = figure('color', 'w', 'position', [50, 50, 400, 400],...
        'menubar', 'none', 'toolbar', 'none', 'name', 'Propagate Eq. Pt.', ...
        'numberTitle', 'off');
    
    h_eigList = uicontrol(h_propFig, 'style', 'listbox', ...
        'units', 'pixels', 'position', [10, 290, 380, 100], ...
        'backgroundcolor', 'white');
    
    str = {};
    for m = 1:length(eigVal)
        if(abs(imag(eigVal(m))) < 1e-12)
            % Real
            if(real(eigVal(m)) < 0), tp = 'Stable'; else, tp = 'Unstable'; end
            str{end+1} = sprintf(['%d) %s (%f), Vec = {%.2f, %.2f, %.2f,'...
                ' %.2f, %.2f, %.2f}'], m, tp, real(eigVal(m)),...
                real(eigVec(:,m)));
        elseif(abs(real(eigVal(m))) < 1e-12)
            % Imaginary
            str{end+1} = sprintf('%d) Center (%fj)', m, imag(eigVal(m)));
        else
            % Mixed
            if(imag(eigVal(m)) < 0), c = '-'; else, c = '+'; end
            if(real(eigVal(m)) < 0), tp = 'Stable'; else, tp = 'Unstable'; end
            str{end+1} = sprintf('%d) %s Mixed (%f) %c %fj', m, tp, ...
                real(eigVal(m)), c, abs(imag(eigVal(m))));
        end
    end
    h_eigList.String = str;
    
    h_dirGroup = uibuttongroup(h_propFig, 'title', 'vx Direction', ...
        'units', 'pixels', 'position', [10, 240, 380, 40], ...
        'backgroundcolor', 'white');
    
    h_posDir = uicontrol(h_dirGroup, 'style', 'radiobutton', ...
        'String', '+x', 'units', 'pixels', ...
        'position', [5, 2, 100, 25], 'backgroundcolor', 'w');
    
    h_negDir = uicontrol(h_dirGroup, 'style', 'radiobutton', ...
        'String', '-x', 'units', 'pixels', ...
        'position', [110, 2, 100, 25], 'backgroundcolor', 'w');
    
    uicontrol(h_propFig, 'style', 'text', 'string', 'TOF (nd):',...
        'units', 'pixels', 'position', [10, 205, 80, 25], ...
        'backgroundcolor', 'white');

    h_tofInput = uicontrol(h_propFig, 'style', 'edit', 'string', '0.00', ...
        'units', 'pixels', 'position', [100, 210, 100, 25]);

    h_evt = uipanel(h_propFig, 'Title', 'Events', 'backgroundcolor', 'w', ...
        'units', 'pixels', 'position', [10, 40, 380, 160]);

    uicontrol(h_evt, 'style', 'pushbutton', 'string', '+',...
        'units', 'pixels', 'position', [5, 110, 50, 25], 'enable', 'off');

    uicontrol(h_evt, 'style', 'pushbutton', 'string', '-',...
        'units', 'pixels', 'position', [65, 110, 50, 25], 'enable', 'off');

    uicontrol(h_evt, 'style', 'listbox', 'string', ...
        {'crash'}, 'units', 'pixels',...
        'position', [5, 5, 370, 100], 'enable', 'off');

    uicontrol(h_propFig, 'style', 'pushbutton', ...
        'string', 'Propagate', 'units', 'pixels', ...
        'position', [150, 10, 100, 25], 'callback', @doProp);

    kids = get(h_propFig, 'children');
    set(kids, 'units', 'normalized');
    kids = get(h_dirGroup, 'children');
    set(kids, 'units', 'normalized');
    kids = get(h_evt, 'children');
    set(kids, 'units', 'normalized');
    
    function doProp(~, ~)
        step = 50; % km
        qEq = plotObj.state(ptIx,:);
        valIx = h_eigList.Value;
        o = h_dirGroup.SelectedObject.String;
        if(strcmp(h_posDir.String, o))
            dir = 1;
        elseif(strcmp(h_negDir.String, o))
            dir = -1;
        else
            error('Unrecognized direction');
        end
        
        if(abs(imag(eigVal(valIx))) < 1e-12)
            q0 = qEq + dir*eigVec(:, valIx).'/norm(eigVec(:, valIx)) *...
                step/sharedData.sysParams.charL * sign(eigVec(1, valIx));
        else
            warning('Equilibria propagation for center and mixed types is not yet supported');
            return;
        end
        
        tof = abs(str2double(h_tofInput.String));
        colors = lines(2);
        if(real(eigVal(valIx)) > 0)
            revTime = 'off';
            col = colors(2,:);  % red for unstable
        else
            revTime = 'on';
            col = colors(1,:);  % blue for stable
        end
        
        q0 = [q0, 1];
        arc = cr3bp_lt_simNL(q0, plotObj.ctrl, plotObj.params, 2112,...
            sharedData.sysParams.P1, sharedData.sysParams.P2, 'tof', tof,...
            'revTime', revTime);
        
        % next step: Add arc to plotted objects
        arcName = inputdlg('Arc name:', 'Filename', [1,40], {'manifold'});
        if(isempty(arcName))
            arcName = {'manifold'};
        end
        
        arc.state = arc.State;
        arc.time = arc.Time;
        arc = rmfield(arc, {'State', 'Time'});
        arc.plotProps = struct('linewidth', 2, 'color', col);
        arc.id = arcName{1};
        arc.name = arcName{1};
        arc.info = arcInfoFcn;
        arc.propagate = arcPropFcn;
        
        sharedData.fcns.addPlotObj(arc);
        sharedData.fcns.updatePlot();
        
        close(h_propFig);
    end
end