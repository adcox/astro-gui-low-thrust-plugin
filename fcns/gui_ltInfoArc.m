function gui_ltInfoArc(data, plotObj, ptIx, linkDynamicsFcn)
% GUI_LTINFOARC display the info box for a low-thrust arc
%
%   gui_ltInfoArc(data, plotObj, ptIx, linkDynamicsFcn) displays information
%   about the object stored in plotObj and a point identified by ptIx. If
%   the linkDynamicsFcn is nonempty, it takes two inputs:
%
%       linkDynamicsFcn(fMag, alpha) where fMag is the low-thrust
%       acceleration magnitude associated with the arc (nd) and alpha is the
%       low-thrust orientation angle (rad)
%
%   The first input, data, is a copy of the sharedData structure
%
%   Author: Andrew Cox
%   Version: September 19, 2018

    % Use default propagate function
    h_propFig = figure('color', 'w', 'position', [50, 50, 500, 300],...
        'menubar', 'none', 'toolbar', 'none', 'name', 'Low-Thrust Info',...
        'numberTitle', 'off');
    
    h_title = annotation(h_propFig, 'textbox', 'edgecolor', 'none',...
            'interpreter', 'none', 'String', plotObj.id, 'fontsize', 18,...
            'horizontalalignment', 'center', 'units', 'pixels', ...
            'position', [10, 260, 480, 40], 'backgroundcolor', 'w');
       
    % Parse data from the C++ structures... sort of hacked together right
    % now. Assume that the control, parameters, and law are the same for
    % all segments
    if(isfield(plotObj, 'SegCtrl'))
        plotObj.params = plotObj.SegCtrl(1).Params;
        plotObj.LawID = plotObj.SegCtrl(1).Type;
    end
    if(isfield(plotObj, 'NodeCtrlState'))
        plotObj.ctrl = plotObj.NodeCtrlState{1};
    end
    
    q0 = plotObj.state(ptIx,:);
    if(length(q0) == 7)
        str = sprintf('$\\vec{q} = \\{%.4f, %.4f, %.4f, %.4f, %4f, %.4f, %.4f\\}$', q0);
    else
        str = sprintf('$\\vec{q}(1:6) = \\{%.4f, %.4f, %.4f, %.4f, %4f, %.4f\\}$', q0(1:6));
    end
    h_qLbl = annotation(h_propFig, 'textbox', 'string', str,...
        'units', 'pixels', 'position', [10, 230, 480, 25],...
        'backgroundcolor', 'white',...
        'horizontalalignment', 'left',...
        'interpreter', 'latex', 'edgecolor', 'none',...
        'fontsize', 12);

    str = {sprintf('t = %f', plotObj.time(ptIx))};
    Hlt = [];
    switch(plotObj.LawID)
        case 2112
            Hnat = -0.5*cr3bp_jacobiAt(q0(1:3), q0(4:6), data.sysParams.mu3B);
            Hlt = cr3bp_lt_getHamiltonian(data.sysParams.mu3B, q0(1:3),...
                q0(4:6), q0(7), plotObj.params(1), plotObj.ctrl(1),...
                plotObj.ctrl(2));
            str{end+1} = sprintf('Hnat = %.4f', Hnat);
            str{end+1} = sprintf('Hlt = %.4f', Hlt);
            str{end+1} = sprintf('Law = %d', plotObj.LawID);
            str{end+1} = sprintf('Alpha = %.2f deg, Beta = %.2f deg', plotObj.ctrl*180/pi);
            str{end+1} = sprintf('Accel. Mag. = %.4e', plotObj.params(1));
        otherwise
            warning('Law type %d is not yet supported');
    end
    str{end+1} = sprintf('TOF = %f', plotObj.time(end) - plotObj.time(1));
    uicontrol(h_propFig, 'style', 'text', 'units', 'pixels', ...
        'position', [10, 40, 450, 180], 'string', str, ...
        'backgroundcolor', 'w', 'horizontalalignment', 'left');

    if(~isempty(linkDynamicsFcn))
        uicontrol(h_propFig, 'style', 'pushbutton', 'units', 'pixels', ...
            'position', [150, 10, 200, 25], 'string', 'Match Dynamics', ...
            'callback', @matchDynCallback);
    end
    
    kids = get(h_propFig, 'children');
    set(kids, 'units', 'normalized');
    set([h_qLbl, h_title], 'units', 'normalized');
    
%% Functions
    function matchDynCallback(~,~)
        if(plotObj.LawID == 2112)
            linkDynamicsFcn(plotObj.params(1), plotObj.ctrl(1), Hlt);
        else
            warning('No clear set of dynamics to match; control law = %d',...
                plotObj.lawID);
        end
    end
end