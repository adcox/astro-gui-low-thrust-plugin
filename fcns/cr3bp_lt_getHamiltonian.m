function H = cr3bp_lt_getHamiltonian(mu, pos, vel, m, f, alpha, beta)
%% CR3BP_LT_GETHAMILTONIAN Compute the hamiltonian
%
%   H = cr3bp_lt_getHamiltonian(mu, pos, vel, m, f, alpha, beta) - computes
%   the Hamiltonian value, H, given the system mass ratio, mu, the position
%   and velocity vectors, thrust magnitude, f, nondimsensional mass, m, 
%   and pointing angles, alpha and beta (in radians).
%
%   
%
    x = pos(:,1);
    y = pos(:,2);
    z = pos(:,3);
    v_squared = sum(vel.*vel,2);
    u = [cos(alpha).*cos(beta), sin(alpha).*cos(beta), sin(beta)];
    
    r13 = sqrt( (x + mu).^2 + y.^2 + z.^2);
    r23 = sqrt( (x -1 + mu).^2 + y.^2 + z.^2);
    
    H = 0.5*v_squared - 0.5*(x.^2 + y.^2) - (1-mu)./r13 - mu./r23 -...
        (f./m) .* sum(pos.*u,2);
end