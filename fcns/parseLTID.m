function ids = parseLTID(id_in)
id = uint16(id_in);

BASE_MASK = bitshift(31, 11, 'uint16');
F_MASK = bitshift(7, 8, 'uint16');
M_MASK = bitshift(3, 6, 'uint16');

base_bin = bitshift(bitand(id, BASE_MASK, 'uint16'), -11, 'uint16');
f_bin = bitshift(bitand(id, F_MASK, 'uint16'), -8, 'uint16');
m_bin = bitshift(bitand(id, M_MASK, 'uint16'), -6, 'uint16');
op_bin = bitget(id, [6:-1:1], 'uint16');

ids = cast([base_bin, f_bin, m_bin, op_bin], 'double');
end
