/*
 *	Copyright 2015-2018, Andrew Cox; Protected under the GNU GPL v3.0
 *	
 *	This file is part of the Low-Thrust plugin for the Astrohelion GUI, 
 *	hereafter refered to as LTPlugin
 *
 *  LTPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LTPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LTPlugin.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * @brief Matlab interface to the C++ file
 * 
 * @param nlhs Number of output arguments (left-hand side = lhs)
 * @param plhs Matrix of output pointers 
 * @param nrhs Number of input arguments (right-hand side = rhs)
 * @param prhs mxArray of input pointers
 * 
 *  For this function, the intput pointers are as follows:
 *  [0]     -   Filepath to originating family
 *  [1] 	- 	Integer identifying what metric to use to identify family members
 *  			0 = get member by alpha (rad)
 *  			1 = get member by H_lt (nondim)
 *  [2] 	- 	Value to identify family member by, i.e., if [1] is 0, then this
 *  			argument specifies the alpha value.
 */
void mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs){

	/* -------------------------------------------------------------------------
     * Parse inputs
     * -------------------------------------------------------------------------
     */
	char *filepath = mxArrayToString(prhs[0]);

	double *tmp = nullptr;
	tmp = mxGetPr(prhs[1]);
	int search_tp = static_cast<int>(tmp);

	tmp = mxGetPr(prhs[2]);
	double val = *tmp;

	/* -------------------------------------------------------------------------
     * Analysis
     * -------------------------------------------------------------------------
     */
	std::vector<ControlLaw *> loadedLaws {};
	SysData_cr3bp_lt sys(filepath);
	Family_PO_cr3bp_lt fam(&sys);
	fam.readFromMat(filepath, loadedLaws);

	char msg[128];
	std::vector<Arcset_periodic> matches {};
	switch(search_tp){
		case 0:
			matches = fam.getMemberBy2DThrustAngle(val);
			break;
		case 1:
			matches = fam.getMemberByH_lt(val);
			break;
		default:
			sprintf(msg, "Unrecognized search_tp = %d", search_tp);
			mexErrMsgTxt(msg);
	}

	/* -------------------------------------------------------------------------
     * Format the outputs
     * -------------------------------------------------------------------------
     */
	const char* fieldnames[] = {"state", "time"};	// TODO: Add more

	// Convert orbit structures to Matlab structures for output
	plhs[0] = mxCreateStructMatrix(matches.size(), 1, nFields, fieldnames);

	// See astro_lowThrustZACs.cpp
}//====================================================

void freeLaws(std::vector<ControlLaw*>& laws){
	for(auto p : laws){
		delete p;
		p = nullptr;
	}
}//====================================================