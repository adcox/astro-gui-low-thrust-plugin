/*
 *	Copyright 2015-2018, Andrew Cox; Protected under the GNU GPL v3.0
 *	
 *	This file is part of the Low-Thrust plugin for the Astrohelion GUI, 
 *	hereafter refered to as LTPlugin
 *
 *  LTPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  LTPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with LTPlugin.  If not, see <http://www.gnu.org/licenses/>.
 *  
 *  
 *  Calling this function
 *  
 *  	data = astro_lowThrustZACs(P1, P2, fMag) computes the ZACs for the CR3BP-LT
 *  	system with primaries P1 and P2 and low-thrust acceleration magnitude
 *  	fMag (nondimensional). The output data structure includes the following
 *  	fields:
 *  	
 *  	data.EqPts 	- 	an nx1 cell array where 3 <= n <= 4 of points on the ZAC
 *  					structures. Each row within a cell array stores
 *  					[alpha, x, y] in rad and nondimensional units.
 *  	
 *  	data.Mu 	- 	mass ratio associated with the P1-P2 CR3BP system
 *  	
 *  	data.fMag 	- 	low-thrust acceleration magnitude, nondim
 *  
 *  Dependencies:
 *  
 *  * Astrohelion library; see <https://gitlab.com/adcox/Astrohelion>
 *  
 */
#include <mex.h>

#include "astrohelion/DynamicsModel_cr3bp_lt.hpp"
#include "astrohelion/Exceptions.hpp"
#include "astrohelion/SysData_cr3bp_lt.hpp"

using namespace astrohelion;

/**
 * @brief Matlab interface to the C++ file
 * 
 * @param nlhs Number of output arguments (left-hand side = lhs)
 * @param plhs Matrix of output pointers 
 * @param nrhs Number of input arguments (right-hand side = rhs)
 * @param prhs mxArray of input pointers
 * 
 *  For this function, the intput pointers are as follows:
 *  [0]     -   Name of P1
 *  [1] 	- 	Name of P2
 *  [2] 	- 	low-thrust acceleration magnitude (nondim)
 */
void mexFunction(int nlhs, mxArray **plhs, int nrhs, const mxArray **prhs){
	(void) nlhs;

	if(nrhs < 3)
		mexErrMsgTxt("Expecting 3 arguments: P1, P2, fMag");

	/* -------------------------------------------------------------------------
     * Convert Matlab input data to C++ structures
     * -------------------------------------------------------------------------
     */
	char *P1 = mxArrayToString(prhs[0]);
	char *P2 = mxArrayToString(prhs[1]);

	double *fMag = mxGetPr(prhs[2]);
	const mwSize* f_dims = mxGetDimensions(prhs[2]);
	int f_len = static_cast<int>(f_dims[1]);
	if(f_len != 1){
		mexErrMsgTxt("Could not parse fMag; too few or too many arguments");
		return;
	}

	// mexPrintf("Computing %s-%s ZAC at f= %.4e\n", P1, P2, *fMag);
	
	/* -------------------------------------------------------------------------
     * Do the analysis
     * -------------------------------------------------------------------------
     */
	SysData_cr3bp_lt sys(P1, P2, 1);
	std::vector< std::vector<double> > LPts(4, std::vector<double>());
	DynamicsModel_cr3bp_lt::getEquilibPt(&sys, 1, *fMag, 1e-6, &(LPts[0]));
	DynamicsModel_cr3bp_lt::getEquilibPt(&sys, 2, *fMag, 1e-6, &(LPts[1]));
	DynamicsModel_cr3bp_lt::getEquilibPt(&sys, 3, *fMag, 1e-6, &(LPts[2]));
	try{
		DynamicsModel_cr3bp_lt::getEquilibPt(&sys, 4, *fMag, 1e-6, &(LPts[3]));
	}catch(const Exception &e){
		mexPrintf("%s\n", e.what());
		LPts.erase(LPts.end());	// Delete the empty fourth ZAC
	}

	/* -------------------------------------------------------------------------
     * Format the outputs
     * -------------------------------------------------------------------------
     */

	const char* fieldnames[3] = {"EqPts", "Mu", "fMag"};

	// size = (1,1), nfields = 2
	plhs[0] = mxCreateStructMatrix(1, 1, 3, fieldnames);

	// Copy ZAC data to mex cell array
	mxArray* zacOut = mxCreateCellMatrix(LPts.size(), 1);
	std::vector<mxArray*> zacs {};	// Storage for mex array pointers
	for(unsigned int i = 0; i < LPts.size(); i++){
		unsigned int nPts = LPts[i].size()/3;
		// Allocate space for this ZAC
		zacs.push_back(mxCreateDoubleMatrix(nPts, 3, mxREAL));
		double *pZAC = mxGetPr(zacs[i]);

		// Assign values to the pointer; data is stored in column-major order, 
		// so rearrange it
		for(unsigned int r = 0; r < nPts; r++){
			for(unsigned int c = 0; c < 3; c++){
				pZAC[c*nPts + r] = LPts[i][3*r + c];
			}
		}

		mxSetCell(zacOut, i, zacs[i]);
	}

	mxArray* muOut = mxCreateDoubleMatrix(1, 1, mxREAL);
	double *temp = mxGetPr(muOut);
	temp[0] = sys.getMu();

	mxArray* fOut = mxCreateDoubleMatrix(1,1, mxREAL);
	temp = mxGetPr(fOut);
	temp[0] = *fMag;

	// Assign the field values
	mxSetFieldByNumber(plhs[0], 0, 0, zacOut);
	mxSetFieldByNumber(plhs[0], 0, 1, muOut);
	mxSetFieldByNumber(plhs[0], 0, 2, fOut);
}//====================================================