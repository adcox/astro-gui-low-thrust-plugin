function  out = plugin_lowThrust(h_tab)
% PLUGIN_SYSINFO Display system info
%
%       out = plugin_sysInfo(h_tab)
%
%   Input arguments:
%       h_tab   -   Handle of the tab that will contain this plugin. If
%                   empty, the initialization terminates and returns the
%                   name of the plugin.
%
%   Output arguments:
%       out     -   Stores a structure that is populated differently for
%                   each initialization of the plugin. If h_tab = [], then
%                   out contains a field called "name" with a
%                   human-readable string to populate the plugin menu. If
%                   h_tab is nonempty, the output includes a handle that
%                   points to the cleanup function that will be called if
%                   and when the plugin is deleted.
%
%   Author: Andrew Cox
%   Version: September 19, 2018
    
    global sharedData;
    
    if(isempty(h_tab))
        out = struct('name', 'Low-Thrust Dynamics');
        return;
    end

    thisFilepath = mfilename('fullpath');
    
    % Add path to MATLAB functions/scripts
	p = getFullPath(sprintf('%s/../fcns', thisFilepath));
    addpath(p);
    
    % Add path to compiled functions
    p = getFullPath(sprintf('%s/../bin', thisFilepath));
    addpath(p);
    
%% Set default values
    col_zac = lines(5);
    stabColors = [1, 0, 0;
                  0, 0, 1;
                  col_zac(5,:);
                  0, 0, 0;
                  1./[sqrt(2), 0, sqrt(2)]];
              
    sharedData.ltParams = struct('fMag', 7e-2, 'alpha', 0, 'beta', 0,...
        'Hlt', -1.5);
    sharedData.ltEqPts = struct('pos', [], 'stab', [], 'Hlt', []);
    sharedData.ltZACs = struct('pos', {{}}, 'stab', {{}}, 'Hlt', {{}},...
        'colorMode', 1,...  % 1 = gray, 2 = by struct, 3 = by stab
        'col_default', col_zac,...
        'col_stab', stabColors);
    
    fig_HltVsAlpha = struct('h_fig', [], 'h_ax', []);
    fig_famBuilder = struct('h_fig', [], 'h_ax', []);
              
    eqPtID = 'LTEqPts';
    zacID = 'LTZAC';
    zvcID = 'LTZVC';
    
%% Set and get properties of the tab
    set(h_tab, 'Title', 'Low-Thrust');        % Set the tab name
    set(h_tab, 'units', 'pixels');
    sz_tab = get(h_tab, 'position');
    
%% Create GUI Elements
    h_enable = uicontrol(h_tab, 'style', 'checkbox', 'units', 'pixels', ...
        'position', [10, sz_tab(4) - 40, 100, 25], 'String', 'Enable', ...
        'backgroundcolor', 'white', 'Value', 0, ...
        'callback', @enableCallback);
    
    h_dynPanel = uipanel(h_tab, 'title', 'View Dynamics', 'units', 'pixels', ...
        'position', [10, sz_tab(4) - 200, sz_tab(3) - 20, 160],...
        'backgroundcolor', 'white');
    
    h_fLbl = annotation(h_dynPanel, 'textbox', 'string', '$a_{lt}$ (nd):',...
        'units', 'pixels', 'position', [5, 105, 80, 25],...
        'backgroundcolor', 'white', 'horizontalalignment', 'left',...
        'interpreter', 'latex', 'edgecolor', 'none', 'fontsize', 12);
    
    h_fInput = uicontrol(h_dynPanel, 'style', 'edit', 'string', sprintf('%.4e', sharedData.ltParams.fMag),...
        'units', 'pixels', 'position', [110, 100, 100, 25], ...
        'callback', @fMagCallback);
    
    h_alphLbl = annotation(h_dynPanel, 'textbox', 'string', '$\alpha$ (deg):',...
        'units', 'pixels', 'position', [5, 75, 80, 25],...
        'backgroundcolor', 'white', 'horizontalalignment', 'left',...
        'interpreter', 'latex', 'edgecolor', 'none', 'fontsize', 12);
       
    h_alphaInput = uicontrol(h_dynPanel, 'style', 'edit', ...
        'string', sprintf('%.2f', sharedData.ltParams.alpha*180/pi),...
        'units', 'pixels', 'position', [110, 70, 100, 25], ...
        'callback', @alphaCallback);
    
    h_zvcToggle = uicontrol(h_dynPanel, 'style', 'checkbox', ...
        'string', 'ZVC at Hlt:', 'units', 'pixels', ...
        'position', [5, 40, 100, 25], 'value', 0,...
        'backgroundcolor', 'w', 'callback', @zvcToggleCallback);
    
    h_HltInput = uicontrol(h_dynPanel, 'style', 'edit', 'units', 'pixels', ...
        'position', [110, 40, 100, 25], 'string', sprintf('%.4f', sharedData.ltParams.Hlt),...
        'callback', @hltCallback);
    
    h_HltPlotToggle = uicontrol(h_dynPanel, 'style', 'pushbutton', 'units', 'pixels', ...
        'position', [5, 5, 150, 25], 'String', 'Show Energy Curves',...
        'callback', @plotHltCallback);
    
    h_zacToggle = uicontrol(h_tab, 'style', 'checkbox', 'units', 'pixels', ...
        'position', [10, sz_tab(4) - 230, 100, 25], 'String', 'View ZACs', ...
        'backgroundcolor', 'white', 'Value', 0, ...
        'callback', @zacToggleCallback);
    
    h_zacColorGroup = uibuttongroup(h_tab, 'units', 'pixels', ...
    	'position', [10, sz_tab(4) - 310, sz_tab(3) - 20, 80],...
    	'Title', 'ZAC color:', 'backgroundcolor', 'white', ...
        'selectionChangedFcn', @zacColorSelect);

    h_zacColorGray = uicontrol(h_zacColorGroup, 'style', 'radiobutton', ...
    	'String', 'gray', 'position', [10, 30, 100, 25], 'value', 1, ...
        'backgroundcolor', 'w');

    h_zacColorStruct = uicontrol(h_zacColorGroup, 'style', 'radiobutton', ...
    	'String', 'by struct.', 'position', [110, 30, 125, 25], ...
    	'value', 0, 'backgroundcolor', 'w');

    h_zacColorStab = uicontrol(h_zacColorGroup, 'style', 'radiobutton', ...
    	'String', 'by stability', 'position', [10, 5, 125, 25], ...
    	'value', 0, 'backgroundcolor', 'w');
    
    h_loadFamBtn = uicontrol(h_tab, 'style', 'pushbutton', 'units', 'pixels', ...
        'position', [10, sz_tab(4) - 340, 125, 25], 'String', 'Load Family',...
        'callback', @loadFamCallback);
    
    h_famBuildBtn = uicontrol(h_tab, 'style', 'pushbutton', 'units', 'pixels',...
        'position', [145, sz_tab(4) - 340, 125, 25], 'String', 'Build Family',...
        'callback', @buildFamCallback);
    
%     uicontrol(h_tab, 'style', 'text', 'string', 'Control law:', ...
%         'units', 'pixels', 'position', [10, sz_tab(4) - 100, 80, 25],...
%         'backgroundcolor', 'white', 'horizontalalignment', 'left');
%     
%     h_lawPt = uicontrol(h_tab, 'style', 'popupmenu', 'units', 'pixels',...
%         'position', [100, sz_tab(4) - 65, 120, 25],...
%         'String', {'General', 'Perp. Vel. (L)', 'Perp. Vel. (R)', ...
%         'Along Vel. (+)', 'Along Vel. (-)'}, ...
%         'Tooltip', 'Select thrust pointing', 'Value', 1, 'enable', 'off');
%     
%     h_lawMass = uicontrol(h_tab, 'style', 'popupmenu', 'units', 'pixels',...
%         'position', [100, sz_tab(4) - 95, 120, 25], ...
%         'String', {'Const. Mass', 'Var. Mass (CSI)'}, ...
%         'Tooltip', 'Select mass variation', 'Value', 1, 'enable', 'off');
%     
%     h_lawThrust = uicontrol(h_tab, 'style', 'popupmenu', 'units', 'pixels', ...
%         'position', [100, sz_tab(4) - 125, 120, 25], ...
%         'String', {'Const. Thrust', 'Var. Thrust'}, ...
%         'Tooltip', 'Select thrust variation', 'Value', 1, 'enable', 'off');
    
    % These objects are disabled by the enable/disable checkbox
    h_disableable = [h_fInput, h_alphaInput, h_zacToggle, h_HltPlotToggle,...
        h_zacColorGray, h_zacColorStruct, h_zacColorStab, h_loadFamBtn,...
        h_zvcToggle, h_HltInput, h_famBuildBtn];
    
    set(h_disableable, 'enable', 'off');
    
    % Set the units to normalized for all children
    kids = get(h_tab, 'children');
    for i = 1:length(kids)
        set(kids(i), 'units', 'normalized');
    end
    set([h_fLbl, h_alphLbl], 'units', 'normalized');
    
    % Set the output
    out = struct('cleanup', @cleanup);
%% Functions

    function cleanup()
    % CLEANUP Delete any allocated memory or Plot Objects specific to this
    % plugin
    %
    %   sharedData = cleanup(sharedData) deletes variables saved to the
    %   sharedData object and deletes Plot Objects. The updated sharedData
    %   object is then returned to update the core interface
    
        % Remove any data stored in sharedData
        sharedData = rmfield(sharedData, {'ltParams', 'ltEqPts', 'ltZACs'});
        
        % Delete all default plotObjects
        sharedData.fcns.deletePlotObj(eqPtID);
        sharedData.fcns.deletePlotObj(zvcID);
        sharedData.fcns.deletePlotObj(zacID);
    end

    function enableCallback(src, ~)
        if(src.Value == 1)
            enable = 'on';
        else
            enable = 'off';
        end
        
        set(h_disableable, 'enable', enable);
        if(~h_zvcToggle.Value)
            set(h_HltInput, 'enable', 'off');
        end
        
        updateData();
    end

    function alphaCallback(src, ~)
        try
            a = str2double(src.String);
            sharedData.ltParams.alpha = a*pi/180;
            updateData();
        catch err
            error('Could not parse alpha = %s\n%s', src.String, err.msgtext);
        end
    end

    function fMagCallback(src, ~)
        try
            f = str2double(src.String);
            if(f < 0)
                error('f must be positive');
            end
            sharedData.ltParams.fMag = f;
            updateData();
        catch err
            error('Could not parse f = %s\n%s', src.String, err.msgtext);
        end
    end

    function zvcToggleCallback(src, ~)
        if(src.Value)
            set(h_HltInput, 'enable', 'on');
        else
            set(h_HltInput, 'enable', 'off');
        end
        updateData();
    end

    function hltCallback(src, ~)
        try
            sharedData.ltParams.Hlt = str2double(src.String);
            updateData();
        catch err
            error('Could not parse Hlt = %s\n%s', src.String, err.msgtext);
        end
    end

    function zacToggleCallback(~, ~)
        updateData();
    end

    function zacColorSelect(~, evt)
        switch(evt.NewValue.String)
            case h_zacColorGray.String
                sharedData.ltZACs.colorMode = 1;
            case h_zacColorStruct.String
                sharedData.ltZACs.colorMode = 2;
            case h_zacColorStab.String
                sharedData.ltZACs.colorMode = 3;
        end
        
        updateData();
    end

    function plotHltCallback(~,~)
        if(isempty(fig_HltVsAlpha.h_fig))
            fig_HltVsAlpha = gui_HltVsAlpha(@closeHltPlotCallback,...
                @linkDynamics);
        else
            set(fig_HltVsAlpha.h_fig, 'visible', 'on');
            movegui(fig_HltVsAlpha.h_fig, 'onscreen');
        end
        
        fig_HltVsAlpha.updateFcn();
        
        function closeHltPlotCallback(~, ~)
        % Delete the extra figure and reset the handles to empty
            delete(fig_HltVsAlpha.h_fig);
            fig_HltVsAlpha.h_fig = [];
            fig_HltVsAlpha.h_ax = [];
        end
    end

    function buildFamCallback(~,~)
        if(isempty(fig_famBuilder.h_fig))
            famDataDir = getFullPath(sprintf('%s/../../../data/families', thisFilepath));
            fig_famBuilder = gui_famBuilder(famDataDir,...
                @closeFamBuilderCallback, @loadFamily);
        else
            set(fig_famBuilder.h_fig, 'visible', 'on');
            movegui(fig_famBuilder.h_fig, 'onscreen');
        end
        
        function closeFamBuilderCallback(~,~)
            delete(fig_famBuilder.h_fig);
            fig_famBuilder.h_fig = [];
            fig_famBuilder.h_ax = [];
        end
    end

    function loadFamCallback(~,~)
        % %%%%%%%%%%%%%%%%%%%%%%%%%
        % Load family from file
        % %%%%%%%%%%%%%%%%%%%%%%%%%
        fp = getFullPath(sprintf('%s/../../../../data', thisFilepath));
        [fname, fpath] = uigetfile('*.mat', 'Select a family file', fp);
        
        if(isequal(fname, 0))
            return;
        end
        
        loadFamily(fullfile(fpath, fname));
    end

    function updateData()
        if(isfield(sharedData, 'sysParams'))
           if(h_enable.Value)
               % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % ZVCs
                % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                sharedData.fcns.deletePlotObj(zvcID);
                if(h_zvcToggle.Value)
                    points = cr3bp_lt_computeZVC_iso(sharedData.sysParams.P1,...
                        sharedData.sysParams.P2, sharedData.ltParams.Hlt,...
                        sharedData.ltParams.fMag, sharedData.ltParams.alpha);
                    zvc.state = [points(:,1:3), zeros(size(points,1),3)];
                    zvc.time = zeros(size(points,1),1);
                    zvc.id = zvcID;
                    zvc.plotProps = struct('linewidth', 2,...
                        'color', [60, 26, 124]/255);
                    zvc.showInList = 'off';
                    zvc.propagate = [];
                    sharedData.fcns.addPlotObj(zvc); 
                end
                
                % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % ZACs
                % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                computeZACs();
                for L = 1:5
                    sharedData.fcns.deletePlotObj(sprintf('%s%d', zacID, L));
                end
                if(h_zacToggle.Value)
                    plotProps = struct('marker', '.', 'color', 200/255*[1,1,1], ...
                        'linestyle', 'none', 'markersize', 8);
                    for L = 1:length(sharedData.ltZACs.pos)
                        pos = sharedData.ltZACs.pos{L}(:,2:3);
                        
                        if(sharedData.ltZACs.colorMode == 2)
                            plotProps.color = sharedData.ltZACs.col_default(L,:);
                        elseif(sharedData.ltZACs.colorMode == 3)
                            plotProps.color =...
                                sharedData.ltZACs.col_stab(sharedData.ltZACs.stab{L},:);
                        end
                        
                        zacObj = struct('state', [pos, zeros(size(pos,1), 4)], ...
                            'time', zeros(size(pos,1),1), 'params', sharedData.ltParams.fMag, ...
                            'name', sprintf('E%d ZAC', L),...
                            'plotProps', plotProps, ...
                            'id', sprintf('%s%d', zacID, L), ...
                            'showInList', 'off');
                        sharedData.fcns.addPlotObj(zacObj);
                    end
                end

                % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % Distinct Equilibria
                % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                computeEqPts();

                % Delete old eqPts
                sharedData.fcns.deletePlotObj(eqPtID);

                % Create new eqPts object
                n = size(sharedData.ltEqPts.pos,1);
                plotProps = struct('marker', 'd', 'color', 'k', ...
                    'markerfacecolor', 'k', 'linestyle', 'none', ...
                    'markersize', 9);
                if(sharedData.ltZACs.colorMode == 2)
                    plotProps.color = sharedData.ltZACs.col_default(1:n,:);
                elseif(sharedData.ltZACs.colorMode == 3)
                    plotProps.color = sharedData.ltZACs.col_stab(sharedData.ltEqPts.stab,:);
                end
                eqPtsObj = struct('state', [sharedData.ltEqPts.pos(:,2:3), zeros(n, 4)],...
                    'time', zeros(n,1),...
                    'ctrl', [sharedData.ltParams.alpha, 0],  'params', sharedData.ltParams.fMag, ...
                    'name', 'CR3BP-LT Equilibria', ...
                    'plotProps', plotProps, 'id', eqPtID, ...
                    'info', @eqPtInfo, 'propagate', @eqPtProp, ...
                    'showInList', 'off');

                % Add after ZACs so that equilibria are plotted after (on top)
                sharedData.fcns.addPlotObj(eqPtsObj);
                
                % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % Extra calls
                % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                if(~isempty(fig_HltVsAlpha.h_fig) && ...
                        strcmpi(fig_HltVsAlpha.h_fig.Visible, 'on'))
                    fig_HltVsAlpha.updateFcn();
                end
                if(~isempty(fig_famBuilder.h_fig) &&...
                    strcmpi(fig_famBuilder.h_fig.Visible, 'on'))
                    fig_famBuilder.updateFcn();
                end
                
                % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % Send data back and update display
                % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                sharedData.fcns.updatePlot();
                
            else % Enable is not selected
                sharedData.fcns.deletePlotObj(eqPtID);
                for L = 1:5
                    sharedData.fcns.deletePlotObj(sprintf('%s%d', zacID, L));
                end
            end
        else
            warning('Cannot enable low-thrust until a system is defined\n');
            set(h_enable, 'Value', 0);
            set(h_disableable, 'enable', 'off');
        end
    end

    function eqPtInfo(plotObj, ptIx)
        q = plotObj.state(ptIx,:);
        [~, ~, ~, ~, Uxx, Uyy, Uzz, Uxy, Uxz, Uyz] = ...
            cr3bp_getPseudoPot(sharedData.sysParams.mu3B, q(1:3)');
    
        A = zeros(6);
        A(4,5) = 2;
        A(5,4) = -2;
        A(1:3, 4:6) = eye(3);
        A(4:6, 1:3) = [Uxx, Uxy, Uxz; Uxy, Uyy, Uyz; Uxz, Uyz, Uzz];
        [~, v] = eig(A, 'vector');

        pureReal = abs(imag(v)) < 1e-12;
        pureImag = abs(real(v)) < 1e-12;
        mixed = ~pureReal & ~pureImag;

        % Figure out which ZAC each equilibrium point is on
        n = size(sharedData.ltEqPts.pos,1);
        allZACIx = zeros(n,1);
        for i = 1:n
            minDist = zeros(3,1);
            for L = 1:length(sharedData.ltZACs.pos)
                r = [sharedData.ltZACs.pos{L}(2,:) - sharedData.ltEqPts.pos(i,2);...
                    sharedData.ltZACs.pos{L}(3,:) - sharedData.ltEqPts.pos(i,3)];
                rMag = sqrt(sum(r.*r,1));
                minDist(L) = min(rMag);
            end
            [~, allZACIx(i)] = min(minDist);
        end
        % If there is more than one equilibrium point on the ZAC, figure
        % out how this one ranks in the energy lineup
        if(sum(allZACIx == allZACIx(ptIx)) > 1)
            H = sharedData.ltEqPts.Hlt(allZACIx == allZACIx(ptIx));
            HltOrder = length(find(H < sharedData.ltEqPts.Hlt(ptIx))) + 1;
            name = sprintf('$E_%d^%d$', allZACIx(ptIx), HltOrder);
        else
            name = sprintf('$E_%d$', allZACIx(ptIx));
        end
        
        h_infoFig = figure('color', 'w', 'position', [50, 50, 400, 300],...
            'menubar', 'none', 'toolbar', 'none',...
            'name', 'Low-Thrust Equilibria Info', 'numberTitle', 'off');

        
        h_title = annotation(h_infoFig, 'textbox', 'edgecolor', 'none',...
            'interpreter', 'latex', 'String', name, 'fontsize', 18,...
            'horizontalalignment', 'center', 'units', 'pixels', ...
            'position', [10, 260, 380, 40], 'backgroundcolor', 'w');

        h_qLbl = annotation(h_infoFig, 'textbox', 'string', ...
            sprintf('$\\vec{r} = \\{%.6f, %.2f, %.2f\\}$', q(1:3)),...
            'units', 'pixels', 'position', [10, 230, 380, 25],...
            'backgroundcolor', 'white', 'horizontalalignment', 'left',...
            'interpreter', 'latex', 'edgecolor', 'none', 'fontsize', 12);

        lbl = {sprintf('Hlt = %.8f', sharedData.ltEqPts.Hlt(ptIx))};
        
        if(sum(pureReal) > 0)
            lbl{end+1} = 'Real Eigenvalues:';
            for i = 1:6
                if(pureReal(i))
                    lbl{end+1} = sprintf('  %.4e', real(v(i)));
                end
            end
        else
            lbl{end + 1} = 'Real Eigenvalues: none';
        end
        if(sum(pureImag) > 0)
            lbl{end+1} = 'Imag Eigenvalues:';
            for i = 1:6
                if(pureImag(i))
                    lbl{end+1} = sprintf('  %.4ej', imag(v(i)));
                end
            end
        else
            lbl{end+1} = 'Imag Eigenvalues: none';
        end
        if(sum(mixed) > 0)
            lbl{end+1} = 'Mixed Eigenvalues:';
            for i = 1:6
                if(mixed(i))
                    if(imag(v(i)) >= 0)
                        c = '+';
                    else
                        c = '-';
                    end
                    lbl{end+1} = sprintf('  %.4e %c %.4ej',...
                        real(v(i)), c, abs(imag(v(i))));
                end
            end
        else
            lbl{end+1} = 'Mixed Eigenvalues: none';
        end
        
        uicontrol(h_infoFig, 'style', 'text', 'units', 'pixels', ...
            'position', [10, 10, 380, 210], 'string', lbl, ...
            'backgroundcolor', 'w', 'horizontalalignment', 'left');

        kids = get(h_infoFig, 'children');
        set(kids, 'units', 'normalized');
        set([h_qLbl, h_title], 'units', 'normalized');
    end

    function eqPtProp(plotObj, ptIx)
        q = plotObj.state(ptIx,:);
        [~, ~, ~, ~, Uxx, Uyy, Uzz, Uxy, Uxz, Uyz] = ...
            cr3bp_getPseudoPot(sharedData.sysParams.mu3B, q(1:3)');
    
        A = zeros(6);
        A(4,5) = 2;
        A(5,4) = -2;
        A(1:3, 4:6) = eye(3);
        A(4:6, 1:3) = [Uxx, Uxy, Uxz; Uxy, Uyy, Uyz; Uxz, Uyz, Uzz];
        [vec, val] = eig(A, 'vector');
        
        gui_ltEqPtPropFig(sharedData, plotObj, ptIx, vec, val, @arcInfo, @arcProp);
    end

    function arcInfo(plotObj, ptIx)
    % ARCINFO A function to display info for a low-thrust arc.
    %   This function can be used for the Plot Objects; additional
    %   arguments specific to the low-thrust environment are included
    
        gui_ltInfoArc(sharedData, plotObj, ptIx, @linkDynamics);
    end

    function arcProp(plotObj, ptIx)
    % ARCPROP A function to propagate a low-thrust arc
    %   This function can be used for the Plot Objects; additional
    %   arguments specific to the low-thrust environment are included

        gui_ltPropArc(sharedData, plotObj, ptIx, @arcInfo, @arcProp);
    end

    function linkDynamics(f, a, H)
    % LINKDYNAMICS Update the dynamics of the system
    %   linkDynamics(f, a, H) takes the thrust magnitue, f (nd), angle, 
    %   a (rad), and low-thrust Hamiltonian, H, and updates the variables 
    %   and input boxes, then calls updateData() to recompute equilibrium 
    %   points and other structures. If any of these variables are empty
    %   arrays, the corresponding quantity in the plugin is NOT updated
    
        if(~isempty(f))
            sharedData.ltParams.fMag = f;
            h_fInput.String = sprintf('%.4e', sharedData.ltParams.fMag);
        end
        if(~isempty(a))
            sharedData.ltParams.alpha = a;
            h_alphaInput.String = sprintf('%2.f', sharedData.ltParams.alpha*180/pi);
        end
        if(~isempty(H))
            sharedData.ltParams.Hlt = H;
            h_HltInput.String = sprintf('%.4f', sharedData.ltParams.Hlt);
        end

        updateData();
    end

    function loadFamily(filepath)
    % LOADFAMILY Load a low-thrust family from a file
    %   
    %   loadFamily(filepath) loads a low-thrust family located at the
    %   specified filepath
    %
    
        famDat = load(filepath);
        if(~isfield(famDat, 'Mass0'))
            error('The family does not appear to be a low-thrust family');
        end
        
        if(~strcmpi(famDat.P1, sharedData.sysParams.P1) ||...
                ~strcmpi(famDat.P2, sharedData.sysParams.P2))
            
            error('Family P1 & P2 do not match the CR3BP definition');
        end
        
        [~,famName] = fileparts(filepath);
        
        % %%%%%%%%%%%%%%%%%%%%%%%%%
        % Select orbits to display
        % %%%%%%%%%%%%%%%%%%%%%%%%%
        numOrbits = 30;
        ix0 = 1;
        ixf = length(famDat.Members);
        if(length(famDat.Members(ix0:end)) < numOrbits)
            idx = ix0:ixf;
        else
            idx = floor(linspace(ix0, ixf, numOrbits-1)).';
            idx = [idx; ixf];
        end
        
        % Rename some fields, sample the data
        famDat.members = famDat.Members(idx);
        famDat = rmfield(famDat, 'Members');
        famDat.id = famName;
        famDat.childIDs = [];
        
        % %%%%%%%%%%%%%%%%%%%%%%%%%
        % Propagate orbits for smooth curves
        % %%%%%%%%%%%%%%%%%%%%%%%%%
        fprintf('Propagating family members...\n');
        allAlpha = [];
        allThrust = [];
        plotProps = struct('linewidth', 1);
        for i = 1:length(famDat.members)
            if(mod(i,10) == 0)
                fprintf('%.2f%%\n', 100*(i/length(idx)));
            end
            m = famDat.members(i);
            famDat.members(i).state = [];
            famDat.members(i).time = [];
            famDat.members(i).thrust = [];
            famDat.members(i).alpha = [];
            famDat.members(i).beta = [];
            
            for s = 1:(size(m.NodeState, 1) - 1)
                if(size(m.SegState{s},1) < 3)
                    % Need to propagate between the end points
                    try
                        o = cr3bp_lt_simNL(m.NodeState(s,:),...
                            m.NodeCtrlState{s},...  % ctrl0
                            m.SegCtrl(s).Params,...  % params
                            m.SegCtrl(s).Type,...       % Type
                            famDat.P1, famDat.P2, 'tf', m.TOF(s),...
                            't0', m.NodeTime(s),...
                            'exitCond', 'none');
                    catch err
                        warning(err.message);
                        continue;
                    end
                else
                    % Already several points between segment ends; skip
                    % propagation and just save those points
                    o = struct('State', m.SegState{s}, 'Time', m.SegTime{s},...
                        'Jacobi', cr3bp_jacobiAt(m.SegState{s}(:,1:3), ...
                        m.SegState{s}(:,4:6), famDat.Mu));
                end

                famDat.members(i).state = [famDat.members(i).state; o.State];
                t = o.Time;
                if(abs(t(1) - m.NodeTime(s)) > 1e-4)
                    t = t + m.NodeTime(s);
                end
                famDat.members(i).time = [famDat.members(i).time; t];
                
                % Compute the thrust magnitude and pointing along each segment
                SegThrust = NaN(size(m.SegState{s},1),1); 
                SegAlpha = NaN(size(m.SegState{s},1),1);
                SegBeta = NaN(size(m.SegState{s},1),1);
                lawIDs = parseLTID(m.SegCtrl(s).Type);
                ctrl = m.NodeCtrlState{s};
                params = m.SegCtrl(s).Params;

                for j = 1:size(o.State,1)
                    [f, u] = cr3bp_lt_getCtrlVals(o.Time(j),...
                        o.State(j,:), ctrl, params, lawIDs,...
                        sharedData.sysParams.charT, sharedData.sysParams.charL);

                    SegThrust(j) = f;
                    SegAlpha(j) = atan2(u(2), u(1));
                    SegBeta(j) = atan2(u(3), sqrt(u(1).^2 + u(2).^2));
                end

                famDat.members(i).thrust = [famDat.members(i).thrust; SegThrust];
                famDat.members(i).alpha = [famDat.members(i).alpha; SegAlpha];
                famDat.members(i).beta = [famDat.members(i).beta; SegBeta];
            end % End of loop through segments
            
            % Add some properties to the family members for plotting and
            % such
            famDat.members(i).id = sprintf('%s_%02d', famName, i);
            famDat.members(i).famID = famDat.id;
            famDat.childIDs = [famDat.childIDs; famDat.members(i).id];
            famDat.members(i).plotProps = plotProps;
            famDat.members(i).info = @arcInfo;
            famDat.members(i).propagate = @arcProp;
            
            allAlpha = [allAlpha; famDat.members(i).alpha];
            allThrust = [allThrust; famDat.members(i).thrust];
        end % End of loop through indices
        
        % Set the dynamics alpha to match the family if only one value is
        % used throughout the family
        if(std(allAlpha) < 1e-3)
            sharedData.ltParams.alpha = mean(allAlpha);
            h_alphaInput.String = sprintf('%.2f', sharedData.ltParams.alpha*180/pi);
        end
        
        % Set the dynamics thrust magnitude to match the family if only one
        % value is used throughout the family
        if(std(allThrust) < 1e-3)
            sharedData.ltParams.fMag = mean(allThrust);
            h_fInput.String = sprintf('%.4e', sharedData.ltParams.fMag);
        end
        
        % %%%%%%%%%%%%%%%%%%%%%%%%%
        % Send data to core interface
        % %%%%%%%%%%%%%%%%%%%%%%%%%
        sharedData.fcns.addPlotObj(famDat);
        updateData();   % Call this to recalculate equilibria and such
    end
end

function computeEqPts()
    global sharedData;
    P1 = sharedData.sysParams.P1;
    P2 = sharedData.sysParams.P2;
    f = sharedData.ltParams.fMag;
    alpha = sharedData.ltParams.alpha;
    
    data = astro_lowThrustZACs(P1, P2, f);
    
    saddles = cr3bp_lt_getZACSaddles(data.Mu);  % Get saddles
    [~, eqPts] = cr3bp_lt_equilib_countDistinct(data, alpha, 'saddles', saddles);
    eqPts = eqPts{1};
    sharedData.ltEqPts.stab = zeros(size(eqPts,1),1);
    
    % Compute low-thrust Hamiltonian value for each equilibria
    n = size(eqPts,1);
    pos = [eqPts(:,2:3), zeros(n,1)];
    sharedData.ltEqPts.Hlt = cr3bp_lt_getHamiltonian(data.Mu, pos,...
        zeros(n,3), 1, f, alpha, 0);
    
    for i = 1:size(eqPts,1)
        [~, ~, ~, ~, Uxx, Uyy, ~, Uxy] = cr3bp_getPseudoPot(data.Mu,...
            [eqPts(i,2); eqPts(i,3); 0]);

        A = zeros(4);
        A(1:2, 3:4) = eye(2);
        A(3:4, 1:2) = [Uxx, Uxy; Uxy, Uyy];
        A(3,4) = 2;
        A(4,3) = -2;

        sharedData.ltEqPts.stab(i) = getStabType(A);
    end
    
    sharedData.ltEqPts.pos = eqPts;
end

function computeZACs()
    global sharedData;
    P1 = sharedData.sysParams.P1;
    P2 = sharedData.sysParams.P2;
    f = sharedData.ltParams.fMag;
    
    data = astro_lowThrustZACs(P1, P2, f);
    
    % Compute the locations of the saddles if they haven't been passed in
    saddles = cr3bp_lt_getZACSaddles(data.Mu);  % Get saddles
    [~,ix] = sort(saddles(:,2));    % Sort by thrust value
    saddles = saddles(ix,:);
    
    % The saddles bound the number of distinct ZAC structures
    if(data.fMag < saddles(1,2))
        % All give equilibrium structures are separate
        L_grid = 1:5;

        % Construct the L5 solutions
        op = [-1;1;-1]*ones(1,size(data.EqPts{4},2));
        data.EqPts{5} = data.EqPts{4}.*op;
    else
        % L3 has merged with L4/L5, so there are only three distinct
        % structures left
        L_grid = 1:3;
    end
    
    sharedData.ltZACs.pos = data.EqPts(L_grid);
    
    % Compute Jacobi constant at each low-thrust equilibria
    sharedData.ltZACs.Hlt = cell(length(L_grid), 1);
    sharedData.ltZACs.stab = cell(length(L_grid),1);
    mu = sharedData.sysParams.mu3B;
    for ix_L = 1:length(L_grid)
        pos = zeros(size(data.EqPts{L_grid(ix_L)},1),3);
        vel = pos;

        pos(:,1:2) = data.EqPts{L_grid(ix_L)}(:,2:3);
        v_squared = sum(vel.*vel, 2);
        a = data.EqPts{L_grid(ix_L)}(:,1);
        beta = zeros(size(a));
        u = [cos(a).*cos(beta), sin(a).*cos(beta), sin(beta)];

        x = pos(:,1);
        y = pos(:,2);
        z = pos(:,3);
        d = sqrt( (x + mu).^2 + y.^2 + z.^2);
        r = sqrt( (x -1 + mu).^2 + y.^2 + z.^2);
        U = (1-mu)./d + mu./r + 0.5*(x.^2 + y.^2);

        sharedData.ltZACs.Hlt{ix_L} = -U + 0.5*v_squared - data.fMag/1 * sum(pos.*u, 2);
        sharedData.ltZACs.stab{ix_L} = zeros(length(sharedData.ltZACs.Hlt{ix_L}), 1);
        
        for i = 1:size(data.EqPts{L_grid(ix_L)},1)
            [~, ~, ~, ~, Uxx, Uyy, ~, Uxy] = cr3bp_getPseudoPot(mu,...
                [x(i); y(i); 0]);
    
            A = zeros(4);
            A(1:2, 3:4) = eye(2);
            A(3:4, 1:2) = [Uxx, Uxy; Uxy, Uyy];
            A(3,4) = 2;
            A(4,3) = -2;

            sharedData.ltZACs.stab{ix_L}(i) = getStabType(A);
        end
    end
end

function stabType = getStabType(A)
    [~, v] = eig(A, 'vector');

    pureReal = abs(imag(v(1:4))) < 1e-12;
    pureImag = abs(real(v(1:4))) < 1e-12;
    mixed = ~pureReal & ~pureImag;

    if(sum(pureReal) == 2 && sum(pureImag) == 2)
        stabType = 1;   % S x C
    elseif(sum(pureImag) == 4)
        stabType = 2;   % C x C
    elseif(sum(mixed) == 4)
        stabType = 3;   % M x M
    elseif(sum(pureReal) == 4)
        stabType = 4;   % S x S
    elseif(sum(pureReal) == 2 && sum(mixed) == 2)
        stabType = 5;   % S x M
    else
        stabType = -1;  % Undefined...
    end
end