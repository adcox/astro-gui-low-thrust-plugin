# Low-Thrust CR3BP Plugin

This plugin facilitates low-thrust trajectory design in the CR3BP. The natural
model is augmented with a low-thrust acceleration term.

## Installation and Dependencies
To install this plugin in the Astrohelion GUI, simply copy the contents of the repository into a folder named `plugin_lowThrust` (this folder name **must** match the plugin filename, here `plugin_lowThrust.m`). Additionally, this plugin relies on compiled scripts to propagate low-thrust dynamics quickly. These compiled scripts rely on the [Astrohelion](https://gitlab.com/adcox/Astrohelion) library, which must be installed separately (see the README file for the Astrohelion library for instructions). Once Astrohelion is installed, you may need to modify the `compileCPP.m` script to point to the correct include and shared library directories.