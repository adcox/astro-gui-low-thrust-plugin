% This script compiles the C++ files in the cpp directory

% Directory where Astrohelion include file directory is
INC_DIR = '/opt/local/include';

% Directory where Astrohelion library objects are
LIB_DIR = '/opt/local/lib';

if(~isfolder('bin'))
    mkdir('bin');
end

% List of C++ functions to compile with Astrohelion
%   These are the filenames without the .cpp extension
astro_cpp = {'astro_lowThrustZACs', 'astro_famFixedAlpha',...
    'astro_famFixedHlt', 'astro_famFromCenter'};


switch(computer)
    case 'GLNXA64'
        for i = 1:length(astro_cpp)
            cmd = sprintf('mex -I%s cpp/%s.cpp -lastrohelion -L%s -output bin/%s',...
                INC_DIR, astro_cpp{i}, LIB_DIR, astro_cpp{i});
            disp(cmd);
            eval(cmd);
        end
    otherwise
        error('The %s operating system is not supported', computer);
end